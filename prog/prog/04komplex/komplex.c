#include<stdio.h>
#include"komplex.h"
#include<math.h>
void komplex_print (char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_new (double x, double y) {
	komplex z = { x, y };
	return z;
}


komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re , a.im + b.im };
	return result;
}

komplex komplex_sub (komplex a, komplex b) {
	komplex result = { a.re - b.re, a.im - b.im};
	return result;
}

int     komplex_equal (komplex a, komplex b, double acc, double eps) {
	int abr_tau = fabs(a.re - b.re) < acc;
	int abi_tau = fabs(a.im - b.im) < acc;
	int abr_eps = 2.0 * fabs(a.re - b.re) /( fabs(a.re) + fabs(b.re)) < eps;
	int abi_eps = 2.0 * fabs(a.im - b.im) /( fabs(a.im) + fabs(b.im)) < eps;
	int eq__tau = abr_tau & abi_tau;
	int eq__eps = abr_eps & abi_eps;
	int result  = eq__tau | eq__eps;	
	return result;
}

komplex komplex_mul (komplex a, komplex b ) {
	double re_result = a.re * b.re - a.im * b.im;
	double im_result = a.re * b.im + a.im * b.re;
	komplex result = {re_result, im_result};
	return result;
}

komplex komplex_div (komplex a, komplex b) {
	double re_result = (a.re * b.re + a.im * b.im);
	double im_result = (a.im * b.re - a.re * b.im);
	re_result /= (b.im * b.im + b.re * b.re);
	im_result /= (b.im * b.im + b.re * b.re);
	komplex result = {re_result, im_result};
	return result;
}

komplex komplex_conjugate (komplex z) {
	komplex result = {z.re, -z.im};
	return result; 
}

double komplex_abs (komplex z) {
	double result = sqrt( pow(z.re,2) + pow(z.im,2) ); 
	return result;
}

komplex komplex_exp (komplex z) {
	komplex result = {exp(z.re) * cos(z.im), exp(z.re) * sin(z.im)};
	return result;
}

komplex komplex_sin (komplex z) {
	komplex result = {sin(z.re) * cosh(z.im), + cos(z.re) * sinh(z.im)};
	return result; 
}

komplex komplex_cos (komplex z) {
	komplex result = {cos(z.re) * cosh(z.im), - sin(z.re) * sinh(z.im)};
	return result; 
}

komplex komplex_sqrt (komplex z) {
	komplex t = {sqrt( (fabs(z.re) + komplex_abs(z)) / 2.0 ), 0};	
	komplex two = {2.0, 0.0};
	komplex b = {0, z.im};
	double z_im_sign;
	double re_result;
	double im_result;
	if(z.im >= 0) {
		z_im_sign = 1;
	}
	else{
		z_im_sign = -1;
	}
	if(z.re >= 0) {
		re_result = t.re;
		im_result = komplex_div(b,komplex_mul(two,t)).im;
	}
	else {
		komplex abss = {komplex_abs(b),0};
		re_result = komplex_div( abss, komplex_mul(two,t) ).re;
		im_result =  z_im_sign * t.re;
	}
	komplex result = {re_result, im_result};
	return result;
}
