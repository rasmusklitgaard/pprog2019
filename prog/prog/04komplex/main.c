#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6

int main(){
	komplex a = {1,2}, b = {3,4};

	printf("\ntesting komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);

		
/* the following is optional */

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'add' passed :) \n");
	else
		printf("test 'add' failed: debug me, please... \n");
	
	printf("\ntesting komplex_sub...\n");
	r = komplex_sub(b,a);
	komplex_set(&R,2.0,2.0);
	komplex_print("a=",a);
        komplex_print("b=",b);
        komplex_print("b-a should   = ", R);
        komplex_print("b-a actually = ", r);

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'sub' passed :) \n");
	else
		printf("test 'sub' failed: debug me, please... \n");	

	printf("\ntesting komplex_mul...\n");
	r = komplex_mul(b,a);
	komplex_set(&R,-5.0,10);
	komplex_print("a=",a);
        komplex_print("b=",b);
        komplex_print("b*a should   = ", R);
        komplex_print("b*a actually = ", r);

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'mul' passed :) \n");
	else
		printf("test 'mul' failed: debug me, please... \n");	


	printf("\ntesting komplex_div...\n");
	r = komplex_div(a,b);
	komplex_set(&R,11.0/25,2.0/25);
	komplex_print("a=",a);
        komplex_print("b=",b);
        komplex_print("b/a should   = ", R);
        komplex_print("b/a actually = ", r);

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'div' passed :) \n");
	else
		printf("test 'div' failed: debug me, please... \n");	


	printf("\ntesting komplex_conjugate...\n");
	r = komplex_conjugate(a);
	komplex_set(&R,1.0, -2.0);
	komplex_print("a=",a);
        komplex_print("a* should   = ", R);
        komplex_print("a* actually = ", r);

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'conjugate' passed :) \n");
	else
		printf("test 'conjugate' failed: debug me, please... \n");	

	printf("\ntesting komplex_exp...\n");
	r = komplex_exp(a);
	komplex_set(&R,-1.13120438, 2.47172667);
	komplex_print("a=",a);
        komplex_print("exp(a) should   = ", R);
        komplex_print("exp(a) actually = ", r);

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'exp' passed :) \n");
	else
		printf("test 'exp' failed: debug me, please... \n");	

	printf("\ntesting komplex_sin...\n");
	r = komplex_sin(a);
	komplex_set(&R,3.165778513216, 1.95960104142);
	komplex_print("a=",a);
        komplex_print("sin(a) should   = ", R);
        komplex_print("sin(a) actually = ", r);

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'sin' passed :) \n");
	else
		printf("test 'sin' failed: debug me, please... \n");	

	printf("\ntesting komplex_cos...\n");
	r = komplex_cos(a);
	komplex_set(&R,2.032723007,-3.05189779915);
	komplex_print("a=",a);
        komplex_print("cos(a) should   = ", R);
        komplex_print("cos(a) actually = ", r);

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'cos' passed :) \n");
	else
		printf("test 'cos' failed: debug me, please... \n");	

	printf("\ntesting komplex_sqrt...\n");
	r = komplex_sqrt(a);
	komplex_set(&R,1.2720196495, 0.786151377757);
	komplex_print("a=",a);
        komplex_print("sqrt(a) should   = ", R);
        komplex_print("sqrt(a) actually = ", r);

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'sqrt' passed :) \n");
	else
		printf("test 'sqrt' failed: debug me, please... \n");	





	return 0;
}
