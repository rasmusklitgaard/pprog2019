#include<stdio.h>
#include<limits.h>
#include<float.h>
int myownequal(double a, double b, double tau, double eps);
int main() {
	
	int i = 1;
        while(1) {
		int j=i+1;
		int k=(j<i);
		if(k)break;
		i++;
        }
        // Now it is no longer true that i+1 > i
        printf("The largest i we can hold is: i = %i\n",i);
        printf("In header file limits.h INT_MAX = %i\n", INT_MAX);

	printf("Finding the smallest epsilon in double format\n");
	double x_double = 1;
	while(1+x_double != 1) {
		x_double /= 2;
	}
	x_double*=2;
	printf("while loop (double)\n");
	printf("x_double = %.25lg\n",x_double);

	double e_double;
	for (e_double = 1; 1+e_double != 1; e_double/=2){}
	e_double*=2;
	printf("e_double = %.25lg\n",e_double);
	printf("for loop (double)\n");
	double f_double = 1;
	do f_double /= 2; while(1+f_double != 1);

	printf("f_double = %.25lg\n",f_double);
	printf("do while loop (double)\n");	
	printf("DBL_EPSILON = %.25lg\n\n\n",DBL_EPSILON);
	printf("Finding the smallest epsilon in float format\n");
	float x_float = 1;                                             	
	while(1+x_float != 1) {
		x_float /= 2;
	}
	x_float*=2;
	printf("while loop (float)\n");
	printf("x_float = %.25f\n",x_float);

	float e_float;
	for (e_float = 1; 1+e_float != 1; e_float/=2){}
	e_float*=2;
	printf("e_float = %.25f\n",e_float);
	printf("for loop (float)\n");
	float f_float = 1;
	do f_float /= 2; while(1+f_float != 1);
	f_float*=2;
	printf("f_float = %.25f\n",f_float);
	printf("do while loop (float)\n");	

	printf("FLT_EPSILON = %.25lg\n\n\n",FLT_EPSILON);


	printf("Finding the smallest epsilon in long double format\n");
	long double x_long_double = 1;                                             	
	while(1+x_long_double != 1) {
		x_long_double /= 2;
	}
	x_long_double*=2;
	printf("while loop (long double)\n");
	printf("x_long_double = %.25Lg\n",x_long_double);

	long double e_long_double;
	for (e_long_double = 1; 1+e_long_double != 1; e_long_double/=2){}
	e_long_double*=2;
	printf("e_long_double = %.25Lg\n",e_long_double);
	printf("for loop (long double)\n");
	long double f_long_double = 1;
	do f_long_double /= 2; while(1+f_long_double != 1);
	f_long_double*=2;
	printf("f_long_double = %.25Lg\n",f_long_double);
	printf("do while loop (long double)\n");	

	printf("LDBL_EPSILON = %.25Lg\n\n\n",LDBL_EPSILON);



	int max = INT_MAX/2.0;
	float sum_up_float = 0.0;
	for (int i = 1 ; i<=max; i++) {
		sum_up_float += 1.0f / i;
	}
	
	float sum_down_float = 0.0;
	for (int i = max ; i>=1; i--) {
		sum_down_float += 1.0f / i;
	}

	double sum_up_double = 0;
	double sum_down_double = 0;

	
	for (int i = 1; i<=max; i++) {
		sum_up_double += 1.0 / i;
	}		
	
	for (int i = max; i>= 1; i--) {
		sum_down_double += 1.0 / i;
	}

	printf("Sum_up_float = %.25f\n",sum_up_float);
	printf("Sum_down_float = %.25f\n",sum_down_float);
	printf("Sum_up_double = %.25lg\n",sum_up_double);
	printf("Sum_down_double = %.25lg\n", sum_down_double);


	double aaa = 2.55;
	double bbb = 2.56;
	double eps = 0.1;
	double tau = 0.01;
	printf("Testing equal: a = %g, b = %g, eps = %g, tau = %g\n",aaa,bbb,eps,tau);
	printf("Expected result: 1, actual result is: %i\n",myownequal(aaa,bbb,tau,eps));
	
return 0;

}

