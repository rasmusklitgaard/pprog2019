#include<stdio.h>
#include<math.h>

double mysin(double t);
int main(void)
{
	double start = -2 * M_PI;
	double end = 2 * M_PI;
	for(double z = start; z<end; z += 0.1)
	{
		printf("%g %g %g\n",z,mysin(z),sin(z));
	}
	return 0;
}
