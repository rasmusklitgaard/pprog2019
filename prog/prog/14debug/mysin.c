#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#define TYPE gsl_odeiv2_step_rkf45
int ode(double t, const double y[], double dydt[], void* params)
{
	dydt[0] = y[1];
	dydt[1] = -y[0];
	return GSL_SUCCESS;
}
double mysin(double t)
{
	if(t<0)
	{
		return -mysin(-t);
	}
	if(t>=2*M_PI) {
		int n = t / (2 * M_PI);
		return mysin(t-n*2*M_PI);
	}

	int n = 2;
	
	gsl_odeiv2_system sys;
	sys.function = ode;
	sys.jacobian = NULL;
	sys.dimension = n;
	sys.params = NULL;

	double hstart = 0.1;
	double epsabs = 1e-6;
	double epsrel = 1e-6;

	gsl_odeiv2_driver* d;
	d = gsl_odeiv2_driver_alloc_y_new(&sys, TYPE,hstart,epsabs,epsrel);
	
	double t0 = 0;
	double y[] = {0, 1};
	gsl_odeiv2_driver_apply(d, &t0, t, y);

	gsl_odeiv2_driver_free(d);
	return y[0];
}
