#include<math.h>
#include<stdio.h>

double exercise1(double t);
double logistic(double t);
double exercise2(double t, double eps, double y_1);

int main () 
{
	// EXERCISE 1
	double tstop = 3;
	for(double t = 0.0; t < tstop; t += 0.02)
	{
		fprintf(stderr,
			"%g %g %g\n",
			t, 
			exercise1(t), 
			logistic(t));
	}
	// EXERCISE 2
	double phistep = 0.1;



	double phi_stop = 10.0 * M_PI;	
	double eps[] = {0.0, 0.0, 0.01};
	double y_1s[] = {0,-0.5,-0.5};
	for (double phi = 0.0; phi < phi_stop; phi += phistep) 
	{
		fprintf(stdout,"%g %g %g %g %g\n", phi, 
					exercise2(phi, eps[0], y_1s[0]),
					exercise2(phi, eps[1], y_1s[1]),
					exercise2(phi, eps[2], y_1s[2]),
					exercise2(23.2,eps[2], y_1s[2]));
					
	}


	return 0;
}

double logistic(double t)
{
	return 1.0 / (1 + exp(-t));
}
