#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int ode_1(double t, 
	  const double y[],
	  double dydt[],
	  void* params) 
{
	dydt[0] = y[0] * (1 - y[0]);	
	return GSL_SUCCESS;
}

double exercise1(double t) 
{
	double hstart = 0.1, epsabs = 1e-5, epsrel = 1e-5;
	double t0 = 0;

	gsl_odeiv2_system sys;
	sys.function = ode_1;
	sys.jacobian = NULL;
	sys.dimension= 1;
	sys.params   = NULL;
	
	gsl_odeiv2_driver* driver;
	driver = gsl_odeiv2_driver_alloc_y_new
			(&sys,
			gsl_odeiv2_step_rkf45,
			hstart,
			epsabs,
			epsrel);
	
	double y[] = {0.5};
	gsl_odeiv2_driver_apply( driver, &t0, t, y); 

	gsl_odeiv2_driver_free(driver);
	
	return y[0];
	
}
