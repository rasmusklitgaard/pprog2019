#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>

int ode_2     (double t, 
		const double y[], 
		double dydt[], 
		void* params)
{
	double* d_params = (double*) params;
	double eps = *d_params;
	dydt[0] = y[1];
	dydt[1] = 1 - y[0] + eps * y[0] * y[0];
	return GSL_SUCCESS;
}

double exercise2 (double t,double eps, double y_1)
{
	double* eps_point = &eps;
	void* params = (void*) eps_point;

	double hstart = 0.1, epsabs = 1e-6, epsrel = 1e-6;

	gsl_odeiv2_system sys;
	sys.function=ode_2;
	sys.jacobian = NULL;
	sys.dimension=2;
	sys.params=params;

	
	gsl_odeiv2_driver* driver;
	driver = gsl_odeiv2_driver_alloc_y_new (&sys,
						gsl_odeiv2_step_rkf45, 
						hstart, 
						epsabs, 
						epsrel);
	double t0 = 0;
	double y[] = {1, y_1};
	gsl_odeiv2_driver_apply(driver, &t0, t, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

