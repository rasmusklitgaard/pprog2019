#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#define TYPE gsl_odeiv2_step_rkf45

int ode(double t, const double y[], double dydt[], void* params)
{
	dydt[0] = (2 / sqrt(M_PI)) * exp(- (t * t));
	return GSL_SUCCESS;
}
double err_fun(double x)
{
	//error function is odd (i think)
	if(x<0) return -err_fun(-x);

	int n = 1;

	// defining system
	gsl_odeiv2_system s;
	s.function = ode;
	s.jacobian = NULL;
	s.dimension = n;
	s.params = NULL;
	
	// setting up driver
	double hstart = 0.01, epsabs = 1e-14, epsrel = 1e-14;
	gsl_odeiv2_driver* d;
	d = gsl_odeiv2_driver_alloc_y_new(&s, TYPE, hstart, epsabs, epsrel);

	double x0 = 0;
	double y[] = {0};
	
	//applying driver
	gsl_odeiv2_driver_apply(d, &x0, x, y);
	gsl_odeiv2_driver_free(d);

	return y[0];	
}

int main(int argc, char** argv)
{
	double a,b,dx;
	if(argc < 4){
		a = -3;
		b = 3;
		dx = 0.1;
	}
	else if(argc > 3) {
		a = atof(argv[1]);
		b = atof(argv[2]);
		dx= atof(argv[3]);
	}

	for(double x = a; x < b; x += dx) {
		printf("%g %g\n",x, err_fun(x));
	}
	return 0;
}
