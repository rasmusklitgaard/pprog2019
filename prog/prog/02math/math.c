#include<stdio.h>
#include<math.h>
#include<complex.h>

int main(void)
{
	double g = tgamma(5);
	double b =   j0(0.5);
	double complex s = csqrt(-2);
	double complex e = cexp(I);
	double complex epi = cexp(I * M_PI);
	double complex ie = cpow(I, M_E);	
	float testf =  0.11111111111111111111111111111111111111111111111;
	double testd = 0.11111111111111111111111111111111111111111111111;
 long double testlong= 0.11111111111111111111111111111111111111111111111L;

	printf("gamma(5) = %g \n", g);	
	printf("bessel(0.5) = %g \n", b);
	printf("sqrt(-2) = %g + %g * I \n", creal(s), cimag(s));
	printf("e^i = %g + %g * I \n", creal(e), cimag(e));
	printf("e^(i*pi) = %g + %g * I  \n", creal(epi), cimag(epi));
	printf("i^e = %g + %g * I \n", creal(ie) , cimag(ie));


	printf("float rep of 0.11111111111111111111111111111111111111111111111 = %.25g\n",testf);
	printf("double rep of 0.11111111111111111111111111111111111111111111111 = %.25lg\n",testd);
	printf("long rep of 0.11111111111111111111111111111111111111111111111 = %.25Lg\n", testlong);

	return 0;
}
