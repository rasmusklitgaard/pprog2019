#include<stdio.h>
void test(void* params)
{
	double* d_params = (double*) params;
	double eps = *d_params;
	printf("eps = %g\n",eps);

}

int main() 
{
	double eps = 0.5;
	double* p_eps = &eps;
	void* v_params = (void*) p_eps;
	test(v_params);
}
