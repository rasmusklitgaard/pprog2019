#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include"rosenbrock.h"
#include"hydrogen.h"
#include<stdio.h>

int main() {
	gsl_vector* root = rosenbrock_root();

	double rmax = 8;
	double energy0 = -1;

	FILE* fp1 = fopen("hydrogen.data","w+");
	for (double rmin = 0.001; rmin < 0.5; rmin += 0.01) {
		fprintf(fp1, "%g %g\n",rmin, hydrogen(rmax, energy0, rmin)); 
	}
	fclose(fp1);

	FILE* fp2 = fopen("h_wav.data","w+");
	double rmin=0.01;
	double best_energy = hydrogen(rmax,energy0,rmin);
	for (double r = 0.01; r<rmax; r += 0.1) 
	{
		double best_e_wf = ode_solution(r, best_energy, rmin);
		#define exact(r) (r)*exp(-(r))
		fprintf(fp2, "%g %g %g\n", r, best_e_wf, exact(r));
	}

	fclose(fp2);

	

	return 0;
}
