#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
#include"rosenbrock.h"

double rosenbrock_function(double x, double y) {
	double result =  (1-x) * (1-x) + 100 * (y - x * x) * (y - x * x);
	return result;
}

int min(const gsl_vector* x, void* params, gsl_vector* f) {
	double xx = gsl_vector_get(x,0);
	double yy = gsl_vector_get(x,1);

	
	double dfdx = 2 * (xx-1) - 400 * (yy - xx * xx) * xx;
	double dfdy = 200 * (yy - xx * xx);
	gsl_vector_set(f, 0, dfdx); 	
	gsl_vector_set(f, 1, dfdy); 	
	return GSL_SUCCESS;
}
gsl_vector* rosenbrock_root() {
	int n = 2;
	gsl_multiroot_fsolver * s;
	s = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids, n);

	gsl_multiroot_function F;
	F.f = min;
	F.n = n;
	F.params = NULL;

	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector_set(x,0,0.0);
	gsl_vector_set(x,1,0.0);
	gsl_multiroot_fsolver_set(s, &F, x);

	int flag;
	double epsabs = 1e-6;
	int iter = 0;
	double resultx,resulty;

	do{
		iter++;
		gsl_multiroot_fsolver_iterate(s);
		flag = gsl_multiroot_test_residual( (*s).f,  epsabs );
		resultx = gsl_vector_get((s->x),0); 
		resulty = gsl_vector_get((s->x),1); 
		printf("%g %g %g\n", resultx, resulty, rosenbrock_function(resultx,resulty));
	} while(flag == GSL_CONTINUE);
	fprintf(stderr, "It took %d iterations to converge\n",iter);
	fprintf(stderr, "Converged on [%g, %g]\n", resultx,resulty);

	gsl_vector* result = gsl_vector_alloc(n);
	gsl_vector_set(result, 0, resultx);
	gsl_vector_set(result, 1, resulty);

	gsl_vector_free(x);
	gsl_multiroot_fsolver_free(s);
	return result;
}
