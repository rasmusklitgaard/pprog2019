#include<stdio.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>
#include"hydrogen.h"
#define TYPE gsl_multiroot_fsolver_hybrids
#define TYPE gsl_multiroot_fsolver_hybrids

struct params {double rmax; double rmin;};

int ode(double r,const double y[], double dydt[], void* params){
	double energy = *(double*)params;
	dydt[0] = y[1];
	dydt[1] = -2 * (1/r + energy) * y[0]; 
	return GSL_SUCCESS;
	
}
double ode_solution(double rmax, double energy, double rmin)
{
	int n = 2;
	void* params = (void*) &energy;
	gsl_odeiv2_system s;
	s.function = ode;
	s.jacobian = NULL;
	s.dimension = n;
	s.params = params;


	double hstart = 0.1, epsabs = 1e-6, epsrel = 1e-6;
	double y0[] = {rmin-rmin*rmin,1-2*rmin}; // FIXME

	gsl_odeiv2_driver* d = gsl_odeiv2_driver_alloc_y_new(	&s,
								gsl_odeiv2_step_rkf45,
								hstart,
								epsabs, epsrel);
	double r = rmin;
	gsl_odeiv2_driver_apply(d, &r, rmax, y0);
	
	gsl_odeiv2_driver_free(d);
	double result = y0[0];
	return result;
}
double aux(double rmax, double energy, double rmin)
{
	return ode_solution(rmax, energy,rmin);
}
int root_equation( const gsl_vector* x, void* params, gsl_vector* f) {
	double energy = gsl_vector_get(x,0);
	struct params par = *(struct params*)params;
	double rmax = par.rmax;
	double rmin = par.rmin;
	double mismatch = aux(rmax,energy,rmin);
	gsl_vector_set(f,0,mismatch);
	return GSL_SUCCESS;
}
double  hydrogen(double rmax,double energy0, double rmin) {
	int n = 1;
	gsl_multiroot_fsolver* s;
	s = gsl_multiroot_fsolver_alloc(TYPE, n);
	struct params par = {rmax, rmin};	
	void* params = (void*) &par;


	gsl_multiroot_function F;
	F.n = n;
	F.f = root_equation;
	F.params = params;

	gsl_vector* start = gsl_vector_alloc(n);
	gsl_vector_set(start,0,energy0);

	gsl_multiroot_fsolver_set(s, &F, start);

	double epsabs = 1e-12;
	int iter = 0;
	int flag;
	do {
		iter++;
		gsl_multiroot_fsolver_iterate(s);
		flag = gsl_multiroot_test_residual( s->f, epsabs );

	} while(flag == GSL_CONTINUE);
		
	double result = gsl_vector_get(s->x, 0);

	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(start);

	return result;
}
