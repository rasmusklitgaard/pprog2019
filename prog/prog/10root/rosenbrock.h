#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
int min(const gsl_vector* x, void* params, gsl_vector* f);
gsl_vector* rosenbrock_root(void);
double rosenbrock(double x, double y);
