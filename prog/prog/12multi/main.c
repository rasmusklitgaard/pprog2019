#include<stdio.h>
#include<pthread.h>
#include"pthreads_funcs.h"
#include"openmp_funcs.h"
int main(void)
{
	int n = 10000000;
	int no_threads = 8;
	for(int i = 0; i<n; i+=100000) 
		printf("%d %g %g\n", i, openmp((long int) i) * 4.0 / i, 4.0 / i * pi_calc_pthread(i,no_threads));
	return 0;
}
