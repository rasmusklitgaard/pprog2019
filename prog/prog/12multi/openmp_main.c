#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include"openmp_funcs.h"

int main(int argc, char** argv)
{
	long int n;
	if(argc>1) n = atoll(argv[1]);
	printf("pi = %g\n",openmp(n) * 4.0 / n);
	return 0;
}
