#include<time.h>
#include<pthread.h>
#include"pthreads_funcs.h"
#include<stdlib.h>
#include<stdio.h>
#include<math.h>

struct pi_cont {int n; int startseed; int hits;};
double pthreads(int n, int no_threads);
int main(int argc, char** argv)
{
	int n,m;
	if(argc>2) {
		n = atoi(argv[1]);	
		m = atoi(argv[2]);
	}
	printf("pi is about: %g\n",pi_calc_pthread(n, m) * 4.0 / n);
	return 0;
}
