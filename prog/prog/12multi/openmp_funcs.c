#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

double openmp(long int n);
double dist2(double x, double y);

double dist2(double x, double y) {
	return sqrt(x*x + y*y);
}

double openmp(long int n)
{
	long int hits = 0;
	unsigned int seed = time(NULL);
	#pragma omp parallel for reduction(+:hits) private(seed)
	for(long int i=0;i<n;i++){
		seed+=i;
		double x = 1.0*rand_r(&seed)/RAND_MAX - 0.5; 
		double y = 1.0*rand_r(&seed)/RAND_MAX - 0.5; 
		if(dist2(x,y)<=0.5) hits++;
	}
	return hits; 
}
