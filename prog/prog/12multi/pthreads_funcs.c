#include"pthreads_funcs.h"
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
struct pi_cont {int n; int startseed; int hits;};
void* bar(void* params) { //does monte carlo simulation from a-b 
	struct pi_cont* cont = (struct pi_cont*) params;
	int n = cont->n;
	int hits = 0;
	unsigned int seed = (unsigned int) cont->startseed; 
	for(int i = 0; i<n; i++) {
		double x = 1.0*rand_r(&seed)/RAND_MAX - 0.5; 
		double y = 1.0*rand_r(&seed)/RAND_MAX - 0.5; 
		if(dist(x,y) <= 0.5) hits++;
	}
	
	cont->hits = hits;
	return NULL;
}
double dist(double x, double y) {
	return sqrt(x*x + y*y);
}
double pi_calc_pthread(int n, int no_threads)
{

	struct pi_cont conts[no_threads];
	pthread_t ts[no_threads];


	
	for(int i = 0; i< no_threads; i++) { // setting up containers
		conts[i].n = n/no_threads;
		conts[i].hits = 0;
		conts[i].startseed = time(NULL) + i * i; 
	}
	
	
	for(int i = 0; i< no_threads; i++) {
		pthread_create(&ts[i],NULL,bar,(void*)(conts+i));
	}
	
	
	for(int i = 0; i< no_threads; i++) {
		pthread_join(ts[i], NULL);
	}
	

	int totalhits = 0;
	
	for(int i = 0; i< no_threads; i++){
		totalhits += conts[i].hits;
	}
	double result = totalhits;
	return result;
}
