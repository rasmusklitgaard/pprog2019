#define TYPE gsl_multimin_fdfminimizer_vector_bfgs2
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>
double rb(const gsl_vector* x, void* params) 
{
	double y = gsl_vector_get(x,0);
	double z = gsl_vector_get(x,1);
	double result = (1-y)*(1-y) + 100 * (z-y*y)*(z-y*y);
	return result;
}
void rb_grad(const gsl_vector* v, void* params, gsl_vector* df) {
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	double fx = 2*(-1+x-200*(y-x*x)*x);
	double fy = 200*(y-x*x);
	gsl_vector_set(df, 0, fx);
	gsl_vector_set(df, 1, fy);
}
void rb_fdf(const gsl_vector* x, void* params, double* f, gsl_vector* df) {
	*f = rb(x,params);
	rb_grad(x,params,df);
}
int main() 
{
	int n = 2;
	gsl_multimin_fdfminimizer* s;
	s = gsl_multimin_fdfminimizer_alloc(TYPE, n);
	
	gsl_vector* start = gsl_vector_alloc(n);	
	gsl_vector_set(start,0, 0);
	gsl_vector_set(start,1, 0);

	double step_size = 0.1;
	gsl_multimin_function_fdf F;
	F.f = rb;
	F.df = rb_grad;
	F.fdf = rb_fdf;
	F.n = n;
	F.params = NULL;
	
	double tolerance = 1e-4;
	gsl_multimin_fdfminimizer_set(s, &F, start, step_size, tolerance);

	int status = 0;
	int iter = 0;
	double epsabs = 1e-5;
	double x,y;
	do
	{
		iter++;
		status = gsl_multimin_fdfminimizer_iterate(s);
		if(status != 0)
			break;
		status = gsl_multimin_test_gradient(s->gradient, epsabs);
		
		x = gsl_vector_get(s->x,0);
		y = gsl_vector_get(s->x,1);

		fprintf(stderr,"%g %g\n",x,y);
		
		if(status == GSL_SUCCESS)
			printf("Converged on minimum at: \n");
		
	} while(status == GSL_CONTINUE);
	printf("x = %g, y = %g\n", x,
				   y);
	printf("Converged after %d steps\n",iter);
	printf("f(xmin,ymin) = %g\n",rb(s->x,NULL));
	return 0;
}
