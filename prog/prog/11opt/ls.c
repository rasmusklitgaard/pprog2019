#include<gsl/gsl_multimin.h>
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#define TYPE gsl_multimin_fminimizer_nmsimplex2

struct data {int n; double *t,*y,*e;};

double fun(double A, double B, double T, double t) 
{
	return A * exp(-t / T) + B;
}
double min_fun(const gsl_vector* x, void* params)
{
	//unpack data
	struct data* data = (struct data*) params;
	int n = data->n; 
	double* t = data->t; 
	double* y = data->y; 
	double* e = data->e; 
	//unpack variables
	double A = gsl_vector_get(x,0);
	double B = gsl_vector_get(x,1);
	double T = gsl_vector_get(x,2);
	double sum = 0;
	for(int i = 0; i<n; i++) {
		sum += pow(( fun(A,B,T,t[i]) - y[i] ),2) / e[i] / e[i];
	}
	return sum;
}
int main(void)
{
	/* This is data */
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);

	//putting data in struct
	struct data datastruct = {n,t,y,e};

	//problem dimension is 3
	int dim = 3;

	gsl_multimin_fminimizer* s;	
	s = gsl_multimin_fminimizer_alloc(TYPE, dim);
	
	//setting function
	gsl_multimin_function F;
	F.f = min_fun;
	F.n = dim;
	F.params = (void*) &datastruct;
	
	//setting start and step
	gsl_vector* start;
	gsl_vector* step;
	start = gsl_vector_alloc(dim);
	step = gsl_vector_alloc(dim);
	
	gsl_vector_set_all(start,0.0);
	gsl_vector_set_all(step,1.0);

	//setting minimizer
	gsl_multimin_fminimizer_set(s, &F, start, step);

	//setting up iteration
	int iter = 0, status;
	double epsabs = 1e-5,size;
	do {
		iter++;
		gsl_multimin_fminimizer_iterate(s);
		size = gsl_multimin_fminimizer_size(s);
		status = gsl_multimin_test_size(size, epsabs);

	} while(status == GSL_CONTINUE);

	double A = gsl_vector_get(s->x,0);
	double B = gsl_vector_get(s->x,1);
	double T = gsl_vector_get(s->x,2);

	printf("Converged to minimum after %d iterations with :\n",iter);		
	printf("(A, B, T) = (%g, %g, %g)\n", A, B, T);

	//printing results
	for(int i = 0; i<n; i++){
		fprintf(stderr, "%g %g %g %g\n", t[i], fun(A,B,T,t[i]), y[i], e[i]);
	}
	gsl_multimin_fminimizer_free(s);
	gsl_vector_free(start);
	gsl_vector_free(step);
	return 0;
}


