#include<stdio.h>
#include<math.h>
#include"funcs.h"
int main() {
	double result1 = ex1();
	
	fprintf(stderr, "First integral evaluates to: %g\n",result1);
	
	double alpha_max = 2;
	for(double alpha = 0.1; alpha < alpha_max; alpha += 0.01) {
		printf("%g %g\n",alpha,ex2(alpha));
	}
		

	return 0;
}
