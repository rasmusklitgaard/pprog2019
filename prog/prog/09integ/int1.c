#include<gsl/gsl_integration.h>
#include<math.h>
#include"funcs.h"
double integral1(double x, void* params) {
	return log(x) / sqrt(x);
}
double ex1(){
	int limit = 500;
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);	
	
	gsl_function func1;
	func1.function = integral1;
	func1.params = NULL;

	double result, abserr;
	double epsabs = 1e-6, epsrel = 1e-6;
	double start = 0, end = 1;
	
	gsl_integration_qags(&func1, start, end, epsabs, epsrel, limit, w, &result, &abserr);



	gsl_integration_workspace_free(w);
	return result;
}

double int_H(double x, void* params){
	double alpha = *(double*) params;
	double res = (-alpha * alpha * x * x / 2 + alpha / 2 + x * x / 2 ) * exp(-alpha * x * x);
	return res; 
}
double int_psi(double x, void* params){
	double alpha = *(double*) params;	
	double res = exp(-alpha*x*x);
	return res;

}
double ex2(double alpha){
	int limit = 500;
	
	gsl_integration_workspace* w = gsl_integration_workspace_alloc(limit);	
	
	void* params = (void*)&alpha;
	gsl_function func2;
	func2.function = int_H;
	func2.params = params;

	double result, abserr;
	double epsabs = 1e-6, epsrel = 1e-6;
	
	gsl_integration_qagi(&func2, epsabs, epsrel, limit, w, &result, &abserr);

	gsl_function func3;
	func3.function = int_psi;
	func3.params = params;
	double result2, abserr2;
	
	gsl_integration_qagi(&func3, epsabs, epsrel, limit, w, &result2, &abserr2);


	gsl_integration_workspace_free(w);
	
	return result/result2;
}
