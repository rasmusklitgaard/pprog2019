#include<gsl/gsl_sf_airy.h>
#include<stdio.h>
#include<stdlib.h>

int main(int argc, char* argv[]) {
	gsl_mode_t mode = 0;
	for (int i = 1; i < argc; i++) {
		double x = atof(argv[i]);
		double ai = gsl_sf_airy_Ai(x, mode);
		double bi = gsl_sf_airy_Bi(x, mode);	
		printf("%g \t %g \t %g\n",x,ai,bi);
	}
	return 0;
}
