#include<gsl/gsl_linalg.h>
#include<stdio.h>
#include<math.h>

int main() {
	const int n = 3;
	const int m = 3;
	double a_data[] = {6.13, -2.90, 5.86,
			   8.08, -6.31, -3.89,
			   -4.36, 1.00, 0.19};
	double b_data[] = {6.23, 5.37, 2.29};
	double c_data[] = {0.0, 0.0, 0.0};
	gsl_matrix A = gsl_matrix_view_array(a_data,n,m).matrix;
	gsl_matrix* AAA = gsl_matrix_alloc(n,m);
	gsl_matrix_memcpy(AAA, &A);

	gsl_vector b = gsl_vector_view_array(b_data,n).vector;
	gsl_vector* x= gsl_vector_alloc(n);

	gsl_linalg_HH_solve(&A, &b, x);
	printf("solution to system: \nx =\n ");
	gsl_vector_fprintf(stdout, x, "[%g]");
	printf("\n");
	
	printf("Testing solution by doing Ax:\n");
	printf("Ax should be:\n");
	double re_data[] = {6.23,5.37,2.29};
	gsl_vector real_re = gsl_vector_view_array(re_data,n).vector;
	gsl_vector_fprintf(stdout, &real_re, "[%g]");

	printf("\nMy result of Ax is:\n");
	gsl_vector y = gsl_vector_view_array(c_data,n).vector;
	gsl_blas_dgemv(CblasNoTrans, 1.0, AAA, x, 0.0, &y);
	gsl_vector_fprintf(stdout, &y, "[%g]");
	gsl_vector_free(x);
	return 0;
}
