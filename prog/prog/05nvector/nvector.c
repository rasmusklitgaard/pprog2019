#include<stdio.h>
#include<stdlib.h>
#include"nvector.h"
#include<math.h>

int double_equal(double a, double b) { 

	double acc = 1e-5;
	int eqt, eqe = 1;
	int eq = 0;
	if(fabs(a-b) >= acc) {
		eqt = 0;
	}
	if ( fabs(a-b) / ( fabs(a) + fabs(b) ) >= acc/2.0 ) {
		eqe = 0;
	}
	if(eqt | eqe) {
		eq = 1;
	}
		
	return eq;
}

nvector* nvector_alloc(int n){
	nvector* v = malloc(sizeof(nvector));
  	(*v).size = n;
  	(*v).data = malloc(n*sizeof(double));
  	if( v== NULL ) {
		 fprintf(stderr,"error in nvector_alloc\n");
	}
  	return v;
}

void nvector_set_zero(nvector* v) {
	for (int i = 0; i < (*v).size ; i++) {
		(*v).data[i] = 0;
	}
}

int nvector_equal (nvector* a, nvector* b) {
	int stat = 1;
	if( (*a).size == (*b).size ) {
		for (int i = 0; i < (*a).size ; i++) {
			if( !double_equal( (*a).data[i], (*b).data[i] ) ) {
				stat = 0;
			}
		}
	} 
	return stat;
}


void nvector_free(nvector* v){
	free(v->data); free(v);
} /* v->data is identical to (*v).data */

void nvector_set(nvector* v, int i, double value){
	(*v).data[i]=value; 
}

double nvector_get(nvector* v, int i){
	return (*v).data[i]; 
}

double nvector_dot_product (nvector* u, nvector* v) {
	double dprd = 0;
	if((*u).size == (*v).size) {
		for (int i = 0; i < (*u).size ; i++) {
			dprd += (*u).data[i] * (*v).data[i];
		}
	}
	return dprd;
}

void nvector_print(char* s, nvector* v) {
	printf("%s [%g", s, (*v).data[0]);
	for (int i = 1; i < (*v).size; i++) {
		printf(", %g", (*v).data[i]);
	}
	printf("]\n");
}

void nvector_add (nvector* a, nvector* b) {
	if( (*a).size == (*b).size ) {
		for (int i = 0; i < (*a).size; i++) {	
			nvector_set(a, i, (*a).data[i] + (*b).data[i]);
		}
	}
}

void nvector_sub (nvector* a, nvector* b) {

	if( (*a).size == (*b).size ) {
		for (int i = 0; i < (*a).size; i++) {	
			nvector_set(a, i, (*a).data[i] - (*b).data[i]);
		}
	}
}
void nvector_scale (nvector* a, double x) {
	for(int i = 0; i < (*a).size;  i++) {
		(*a).data[i] *=x;
	}
}
/* ... */










