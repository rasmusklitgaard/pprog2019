
main: testing nvector_alloc ...
test passed

main: testing nvector_set and nvector_get ...
test passed

main: testing nvector_add ...
a+b should   =  [1.17748, 1.71009, 0.532774, 1.046, 1.03137]
a+b actually =  [1.17748, 1.71009, 0.532774, 1.046, 1.03137]
test passed

main: testing nvector_sub ...
a-b should   =  [0.264086, -0.438829, 0.280483, 0.575694, 0.590668]
a-b actually =  [0.264086, -0.438829, 0.280483, 0.575694, 0.590668]
test passed

main: testing nvector_dot_product ...
a*b should   =0.432011 
a*b actually =0.432011 
test passed

main: testing nvector_scale ...
alpha*a should   =  [0.514074, 0.248404, 0.534978, 0.439936, 0.414172]
alpha*a actually =  [0.514074, 0.248404, 0.534978, 0.439936, 0.414172]
test passed

main: testing nvector_set_zero ...
a set to zero should   =  [0, 0, 0, 0, 0]
a set to zero actually =  [0, 0, 0, 0, 0]
test passed

main: testing nvector_equal ...
a =  [0, 0, 0, 0, 0]
b =  [0.137232, 0.156679, 0.12979, 0.998925, 0.512932]
a should not equal b
test passed
