#include<stdio.h>
#include<math.h>
#include"nvector.h"
#include<stdlib.h>
#define RND (double)rand()/RAND_MAX
int main()
{
	int n = 5;

	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if (double_equal(vi, value)) {
		printf("test passed\n");
	}
	else {
		printf("test failed\n");
	}

	printf("\nmain: testing nvector_add ...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);

	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
	}
	nvector_add(a, b);
	nvector_print("a+b should   = ", c);
	nvector_print("a+b actually = ", a);

	if (nvector_equal(c, a)) {
		printf("test passed\n");
	}
	else {
		printf("test failed\n");
	}


	printf("\nmain: testing nvector_sub ...\n");
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x - y);
	}
	nvector_sub(a, b);
	nvector_print("a-b should   = ", c);
	nvector_print("a-b actually = ", a);

	if (nvector_equal(c, a)) {
		printf("test passed\n");
	}
	else {
		printf("test failed\n");
	}


	printf("\nmain: testing nvector_dot_product ...\n");
	double dot_ab_real = 0;
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		dot_ab_real += x*y;
	}
	double dot_ab = nvector_dot_product(a, b);
	printf("a*b should   =%g \n", dot_ab_real);
	printf("a*b actually =%g \n", dot_ab);

	if (double_equal(dot_ab, dot_ab_real)) {
		printf("test passed\n");
	}
	else {
		printf("test failed\n");
	}


	printf("\nmain: testing nvector_scale ...\n");
	double alpha = RND;
	for (int i = 0; i < n; i++) {
		double x = RND; 
		nvector_set(a, i, x);
		nvector_set(c, i, alpha * x);
	}
	nvector_scale(a, alpha);
	nvector_print("alpha*a should   = ", c);
	nvector_print("alpha*a actually = ", a);

	if (nvector_equal(c, a)) {
		printf("test passed\n");
	}
	else {
		printf("test failed\n");
	}



	printf("\nmain: testing nvector_set_zero ...\n");
	for (int i = 0; i < n; i++) {
		double x = RND;
		nvector_set(a, i, x);
		nvector_set(c, i, 0.0);
	}
	nvector_set_zero(a);
	nvector_print("a set to zero should   = ", c);
	nvector_print("a set to zero actually = ", a);

	if (nvector_equal(c, a)) {
		printf("test passed\n");
	}
	else {
		printf("test failed\n");
	}

	printf("\nmain: testing nvector_equal ...\n");
	nvector_print("a = ",a);
	nvector_print("b = ",b);
	
	printf("a should not equal b\n");
	if(nvector_equal(a,b)) {
		printf("test failed\n");
	}
	else{
		printf("test passed\n");
	}


	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);

	return 0;
}
