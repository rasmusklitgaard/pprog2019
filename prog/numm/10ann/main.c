#include"ann.h"
#include<gsl/gsl_vector.h>
#include<math.h>
#include<stdio.h>

int main() {
	int number_of_neurons = 3;
	int N = 8;
	ann* network = ann_alloc(number_of_neurons, &activation_function);

	gsl_vector* xlist = gsl_vector_alloc(N);
	gsl_vector* ylist = gsl_vector_alloc(N);

	double xi;
	double yi;	
	FILE *fp = fopen("training_data.txt","w");
	for(int i = 0; i<N; i++) {
		xi = i*2*M_PI/N;
		yi = sin(xi);
		gsl_vector_set(xlist,i,xi);
		gsl_vector_set(ylist,i,yi);
		fprintf(fp,"%g %g\n",xi,yi);
	}
	fclose(fp);

	fprintf(stderr,"Initializing neural network\n");
	gsl_vector_set(network->data,0,2.5);
	gsl_vector_set(network->data,1,2.5);
	gsl_vector_set(network->data,2,-2.5);
	gsl_vector_set(network->data,3,-2.5);
	gsl_vector_set(network->data,4,2.5);
	gsl_vector_set(network->data,5,2.5);
	gsl_vector_set(network->data,6,13);
	gsl_vector_set(network->data,7,13);
	gsl_vector_set(network->data,8,1);
	fprintf(stderr,"Initial parameters:\n");
	print_params(network->data);

	fprintf(stderr,"\nTraining on tabulated sin values\n");
	ann_train(network, xlist, ylist);
	
	double val, x, der, anti_der, y1, y2;
	for(x = 0; x<M_PI*2; x+=0.1) {
		val = ann_feed_forward(network,	x);
		der = ann_feed_forward_der(network,x);	
		anti_der = ann_feed_forward_anti_der(network,x,M_PI/2);
		y1 = cos(x);
		y2 = -cos(x);
		printf("%g %g %g %g %g %g\n",x,val,der,y1,anti_der,y2);
	}
	fprintf(stderr,"Training done\n");

	fprintf(stderr,"Final parameters:\n");
	print_params(network->data);

	fprintf(stderr,"\nThe approximate function solution can be seen in plot.svg, along with\n");
	fprintf(stderr,"an approximation of the integral and the derivative.\n");

	gsl_vector_free(xlist);
	gsl_vector_free(ylist);
	
	ann_free(network);
	return 0;
}
