#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"qr.h"
void vector_print(gsl_vector* b)
{
	printf("(%li, 1) vector\n",b->size);
	int n = b->size;
	for(int i = 0; i<n; i++)
	{
		double bi = gsl_vector_get(b,i);
		if(bi < 1e-10) bi = 0;
		printf("%6.2g ",bi);
	}
	printf("\n");
	printf("\n");
}
void matrix_print(gsl_matrix* A)
{
	int n = A->size1;
	int m = A->size2;	
	printf("(%i, %i) matrix\n",n,m);
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			double Aij = gsl_matrix_get(A,i,j);
			if(Aij < 1e-10) Aij = 0;
			printf("%6.2g ",Aij);
		}
		printf("\n");
	}
	printf("\n");
}
int main(void)
{
	int n = 9, m = 6;
	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_matrix* B = gsl_matrix_alloc(n,n);
	gsl_matrix* R1 = gsl_matrix_alloc(m,m);
	gsl_matrix* R2 = gsl_matrix_alloc(n,n);
	gsl_matrix* A_copy = gsl_matrix_alloc(n,m);
	gsl_matrix* B_copy = gsl_matrix_alloc(n,n);
	gsl_matrix* QR1 = gsl_matrix_alloc(n,m);	
	gsl_matrix* QR2 = gsl_matrix_alloc(n,n);	
	gsl_matrix* QtQ = gsl_matrix_alloc(m,m);	
	gsl_vector* b = gsl_vector_alloc(n);	
	gsl_vector* x = gsl_vector_alloc(n);	
	gsl_vector* y = gsl_vector_alloc(n);	

	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			double B_ij = ((double) random())/RAND_MAX * 50;
			gsl_matrix_set(B,i,j,B_ij);
		}
	}
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			double A_ij = ((double) random())/RAND_MAX * 50;
			gsl_matrix_set(A,i,j,A_ij);
		}
		double b_i = ((double) random())/RAND_MAX * 50;
		gsl_vector_set(b,i,b_i);
	}
	gsl_matrix_set_zero(R1);
	gsl_matrix_set_zero(R2);

	gsl_matrix_memcpy(A_copy,A);
	gsl_matrix_memcpy(B_copy,B);


	printf("______________________________________________________________\n");
	printf("Testing qr_gs_decomp function: n = %i, m = %i\n \n",n,m);	
	printf("matrix A :\n");
	matrix_print(A);
	printf("matrix R :\n");
	matrix_print(R1);
	printf("Doing QR decomposition \n");
	qr_gs_decomp(A,R1); // FROM NOW ON A IS Q
	printf("matrix Q1 :\n");
	matrix_print(A);
	printf("matrix R :\n");
	matrix_print(R1);


	int flag = 0;
	for(int i = 0; i<m; i++)	
	for(int j = 0; j<m; j++){
		if(j<i)
		if(gsl_matrix_get(R1,i,j) != 0) flag++;
	}
	if(flag) 
	{
		printf("R is not upper/right triangular:");
		printf(" %i of %i lower elements are nonzero\n",flag,(m*m-m)/2);
	}
	else printf("R has been tested and is upper/right triangular\n");

	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R1,0.0,QR1);
	printf("QR:\n");
	matrix_print(QR1);
	
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,QtQ);
	printf("Q.T*Q\n");
	matrix_print(QtQ);
	printf("______________________________________________________________\n");
	printf("Testing qr_gs_solve function: square matrix, n = %i \n\n",n);	

	printf("B\n");
	matrix_print(B);

	printf("x:\n");
	vector_print(x);

	printf("b:\n");
	vector_print(b);

	// for this section use: do QR on B, to get R2
	qr_gs_decomp(B,R2);
	qr_gs_solve(B,R2,b,x);
	printf("solution x to QRx = b\n");
	vector_print(x);
	
	printf("is Bx = b\n");
	gsl_blas_dgemv(CblasNoTrans,1.0,B_copy,x,0.0,y);
	vector_print(y);
	
	printf("______________________________________________________________\n");
	printf("Testing qr_gs_inverse function: square matrix, n = %i \n\n",n);	
	printf("Inverse of B:\n");
	qr_gs_inverse(B, R2, QR2);
	matrix_print(QR2);
	
	printf("B^-1 * B:n\n Should be identity matrix: \n");
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, QR2, B_copy,0.0, R2);
	matrix_print(R2); 

	gsl_matrix_free(A);
	gsl_matrix_free(R1);
	gsl_matrix_free(R2);
	gsl_matrix_free(QR1);
	gsl_matrix_free(QR2);
	gsl_matrix_free(QtQ);
	gsl_matrix_free(A_copy);
	gsl_matrix_free(B_copy);
	gsl_matrix_free(B);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_vector_free(y);
	
	return 0;
}
