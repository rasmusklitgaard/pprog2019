/*
int driver2(
	double* t,                      
	double b,                        
	double* h,                        
	gsl_vector*  yt,                   
	gsl_matrix* path,		    
	double acc,                          
	double eps,                           
	void stepper(                          
		double t, double h, gsl_vector*yt,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*yth, gsl_vector*err
		),
	void f(double t, gsl_vector* y, gsl_vector* dydt) );
int driver(
	double* t,                             
	double b,                             
	double* h,                           
	gsl_vector*yt,                      
	double acc,                        
	double eps,                       
	void stepper(double t, double h, gsl_vector*yt,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*yth, gsl_vector*err),
	void f(double t,gsl_vector* y,gsl_vector* dydt) );
*/
int driver(
	double b,
	double* h,
	gsl_vector* yt,
	double acc,
	double eps,
	void stepper(
		double t, double h, gsl_vector* yt,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err),
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* t_steps,
	gsl_matrix* y_steps,
	int max);
void vector_print(gsl_vector* b);

void rkstep45( double t, double h, gsl_vector* yt,                      
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* yth,                               
	gsl_vector* err);
double calc_norm(gsl_vector* v);
