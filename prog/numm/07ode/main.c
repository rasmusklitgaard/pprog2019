#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"ode.h"
#include<assert.h>
#define M_PI 3.14159
void ode_test(double t, gsl_vector* y, gsl_vector* dydt);
void ode_sin(double t, 
	  gsl_vector* y,
	  gsl_vector* dydt );
void ode_exp(double t, 
	  gsl_vector* y,
	  gsl_vector* dydt );
int main(void)
{
	int n = 2;
	int nexp = 1;
	int max = 10000;
	gsl_vector* y_sin = gsl_vector_alloc(n);
	gsl_vector* y_exp = gsl_vector_alloc(nexp);
	gsl_vector* tsteps_sin = gsl_vector_alloc(max);
	gsl_matrix* ysteps_sin = gsl_matrix_alloc(max,n);
	gsl_vector* tsteps_exp = gsl_vector_alloc(max);
	gsl_matrix* ysteps_exp = gsl_matrix_alloc(max,nexp);


	gsl_vector_set(y_sin,0,0.0);
	gsl_vector_set(y_sin,1,1.0);

	gsl_vector_set(tsteps_sin,0,0);
	gsl_matrix_set(ysteps_sin,0,0,0.0);
	gsl_matrix_set(ysteps_sin,0,1,1.0);

	gsl_vector_set(tsteps_exp,0,0.0);
	gsl_matrix_set(ysteps_exp,0,0,1.0);
	gsl_vector_set(y_exp,0,1.0);

	double b = 5;
	double h = 0.1;
	double acc = 1e-4;
	double eps = 1e-4;
	
	int iter_sin = driver(b,&h,y_sin,acc,eps,rkstep45,ode_sin,tsteps_sin,ysteps_sin,max);
	int iter_exp = driver(b,&h,y_exp,acc,eps,rkstep45,ode_exp,tsteps_exp,ysteps_exp,max);
	
	double t_start_exp = gsl_vector_get(tsteps_exp,0);
	double y_start_exp = gsl_matrix_get(ysteps_exp,0,0);
	double t_end_sin = gsl_vector_get(tsteps_sin,  iter_sin-1);
	double y_end_sin = gsl_matrix_get(ysteps_sin,iter_sin-1,0);

	if(iter_sin > 0) {
		for(int i = 0; i < iter_sin; i++) {
			double ti_sin = gsl_vector_get(tsteps_sin,i);
			double yi_sin = gsl_matrix_get(ysteps_sin,i,0);
			printf("%g %g %g %g\n",ti_sin,yi_sin,t_start_exp,y_start_exp);
		}
	}
	else {
		printf("Driver uses more than max = %d steps when solving sin(x). Exiting\n",max);
		exit(0);
	}
	if(iter_exp > 0) {
		for(int i = 0; i < iter_sin; i++) {
			double ti_exp = gsl_vector_get(tsteps_exp,i);
			double yi_exp = gsl_matrix_get(ysteps_exp,i,0);
			printf("%g %g %g %g\n",t_end_sin,y_end_sin,ti_exp,yi_exp);
		}
	}
	else {
		printf("Driver uses more than max = %d steps when solving exp(x). Exiting\n",max);
		exit(0);
	}

	fprintf(stderr,"Note sine and exponential function plots are made using only one call");
       	fprintf(stderr,"to driver. This means part b) is done, since the path taken comes from"); 
	fprintf(stderr,"running driver.\n");

	gsl_vector_free(y_sin);
	gsl_vector_free(tsteps_sin);
	gsl_matrix_free(ysteps_sin);
	gsl_vector_free(y_exp);
	gsl_vector_free(tsteps_exp);
	gsl_matrix_free(ysteps_exp);
	return 0;
}
void ode_sin(double t, 
	  gsl_vector* y,
	  gsl_vector* dydt ) 
{
	int n = y->size;
	assert(n==2);
	gsl_vector_set(dydt,0,gsl_vector_get(y,1));
	gsl_vector_set(dydt,1,-gsl_vector_get(y,0));
}
void ode_exp(double t,
		gsl_vector* y,
		gsl_vector* dydt )
{
	int n = y->size;
	assert(n==1);
	gsl_vector_set(dydt,0,gsl_vector_get(y,0));
}
