#include<gsl/gsl_vector.h>
void plainmc( 
		double f(gsl_vector* x), 
		gsl_vector* a, 
		gsl_vector* b, 
		int N, 
		double* res, 
		double* err);
void vect_init_rand(gsl_vector* x, gsl_vector* from, gsl_vector* to, unsigned int* seed);
void vector_print(gsl_vector* b);
