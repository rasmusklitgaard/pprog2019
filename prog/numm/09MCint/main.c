#include"montecarlo.h"
#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>

void plainmc( 
		double f(gsl_vector* x), 
		gsl_vector* a, 
		gsl_vector* b, 
		int N, 
		double* res, 
		double* err);
double f1(gsl_vector* x)
{
	assert(x->size == 1);
	double x1 = gsl_vector_get(x,0);
	return x1*x1;
}
double f2(gsl_vector* x)
{
	assert(x->size == 3);
	double x1 = gsl_vector_get(x,0);
	double x2 = gsl_vector_get(x,1);
	double x3 = gsl_vector_get(x,2);
	double val = 1/pow(M_PI,3) / (1-cos(x1)*cos(x2)*cos(x3));
	return val;
}
int main(void)
{
	int n1 = 1;
	int n2 = 3;
	gsl_vector* a1 = gsl_vector_alloc(n1);
	gsl_vector* a2 = gsl_vector_alloc(n2);
	gsl_vector* b1 = gsl_vector_alloc(n1);
	gsl_vector* b2 = gsl_vector_alloc(n2);

	gsl_vector_set(a1,0,-1);
	gsl_vector_set(b1,0,1);
	gsl_vector_set_all(a2,0);
	gsl_vector_set_all(b2,M_PI);
	
	double res1,res2,err1,err2;
	int N1 = 1e+6;
	int N2 = 1e+6;
	
	plainmc(f1,a1,b1,N1,&res1,&err1);
	plainmc(f2,a2,b2,N2,&res2,&err2);

	printf("Integral of x^2:\n");
	printf("from\n");
	vector_print(a1);
	printf("to\n");
	vector_print(b1);
	printf("integral = %g pm %g\n",res1,err1);
	printf("using N = %d\n",N1);
	
	printf("\n");

	printf("Integral of insane function:\n");
	printf("from\n");
	vector_print(a2);
	printf("to\n");
	vector_print(b2);
	printf("integral = %g pm %g\n",res2,err2);
	printf("using N = %d\n",N2);

	// Checking error dependency
	double err11,err22;
	int iterations = 1e+5;
	for(int i = 1; i<iterations; i+=100)
	{
		plainmc(f1,a1,b1,i,&res1,&err11);
		plainmc(f2,a2,b2,i,&res2,&err22);
		fprintf(stderr,"%d %g %g\n",i,err11,err22);
	}

	gsl_vector_free(a1);
	gsl_vector_free(a2);
	gsl_vector_free(b1);
	gsl_vector_free(b2);
	return 0;
}
