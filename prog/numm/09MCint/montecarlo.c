#include<stdlib.h>
#include<stdio.h>
#include<assert.h>
#include<time.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include"montecarlo.h"
void plainmc( 
		double f(gsl_vector* x), 
		gsl_vector* a, 
		gsl_vector* b, 
		int N, 
		double* res, 
		double* err)
{
	int n = a->size;
	gsl_vector* randx = gsl_vector_alloc(n);
	double sum = 0;
	double sum2 = 0;
	double vol = 1;
	for(int i = 0; i<n; i++) {
		vol *= (gsl_vector_get(b,i) - gsl_vector_get(a,i));
	}
	unsigned int seed = time(NULL);
	for(int i = 0; i<N; i++)
	{
		vect_init_rand(randx,a,b,&seed);
		double f_i = f(randx);
		sum += f_i;
		sum2 += f_i*f_i;
	}
	double mean = sum/N;
	double sigma = sqrt(sum2/N - mean*mean);
	double SIGMA = sigma/sqrt(N);
	*err = SIGMA * vol;
	*res = mean  * vol;

	gsl_vector_free(randx);
}

void vect_init_rand(gsl_vector* x, gsl_vector* from, gsl_vector* to, unsigned int* seed)
{
	int n = x->size;
	assert(n == from->size);
	assert(n == to->size);
	for(int i = 0; i<n; i++) {
		double start = gsl_vector_get(from,i);
		double stop  = gsl_vector_get(to,i);
		double a_i = (double) rand_r(seed) / RAND_MAX; // random number between 0 and 1
		a_i = a_i * (stop - start) + start; // now between start and stop
		gsl_vector_set(x,i,a_i);
	}
}
void vector_print(gsl_vector* b)
{
	printf("(%li, 1) vector\n",b->size);
	int n = b->size;
	for(int i = 0; i<n; i++)
	{
		double bi = gsl_vector_get(b,i);
		if(fabs(bi) < 1e-12) bi = 0;
		printf("%7.7g ",bi);
	}
	printf("\n");
	printf("\n");
}
