#include<time.h>
#include<stdio.h>
#include<stdlib.h>
#include"eigen.h"
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<assert.h>
void matrix_init_sym(gsl_matrix* A, double maxval,unsigned int seed);
int main(void)
{	
	int npoints = 130;
	double maxval = 100;
	clock_t start,end;
	double t;
	unsigned int seed = time(NULL);
	int i;
	for(i = 0; i < npoints; i++ )
	{
		seed += i;
		gsl_matrix* A = gsl_matrix_alloc(i,i);
		gsl_matrix* vecs = gsl_matrix_alloc(i,i);
		gsl_vector* vals = gsl_vector_alloc(i);
		matrix_init_sym(A,maxval,seed);
		start = clock();
		jcbi_diag(A,vals,vecs); 
		end = clock();
		t = ( (double) (end-start) ) / CLOCKS_PER_SEC;
		printf("%d %g\n",i,t*1000);
		gsl_vector_free(vals);
		gsl_matrix_free(A);			
		gsl_matrix_free(vecs);			
	}
	return 0;
}
void matrix_init_sym(gsl_matrix* A, double maxval,unsigned int seed)
{
	int n = A->size1;
	assert(A->size2 == n);
	for(int i = 0; i < n; i++)
	for(int j = i; j < n; j++)
	{
		double rv = maxval * ((double) rand_r(&seed)) / RAND_MAX; 
		//double rv = ((double) random())/RAND_MAX * maxval;
		gsl_matrix_set(A,i,j,rv);
		gsl_matrix_set(A,j,i,rv);
	}
}
