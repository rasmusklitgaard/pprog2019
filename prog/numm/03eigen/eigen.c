#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"eigen.h"
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

int jcbi_diag(gsl_matrix* A, gsl_vector* vals, gsl_matrix* V)
{
	int n = A->size1;
	int m = A->size2;
	int flag;
	for(int l = 0; l < n; l++) {
		gsl_vector_set(vals,l,gsl_matrix_get(A,l,l)); 
	}
	gsl_matrix_set_identity(V);
	int iter = 0;
	do{
		iter++;
		flag = 0;
		for (int p = 0; p < n ; p++)
		for (int q = p+1; q < m ; q++)
		{
			double App = gsl_vector_get(vals,p);
			double Aqq = gsl_vector_get(vals,q);
			double Apq = gsl_matrix_get(A,p,q);
			double phi = atan2(2*Apq,Aqq-App)*0.5;
			double c = cos(phi), s = sin(phi);
			double App_n = c*c*App - 2*s*c*Apq + s*s*Aqq;
			double Aqq_n = s*s*App + 2*s*c*Apq + c*c*Aqq;
			if(App_n != App || Aqq_n != Aqq)
			{
				flag = 1;
				gsl_vector_set(vals,p,App_n);
				gsl_vector_set(vals,q,Aqq_n);
				gsl_matrix_set(A,p,q,0.0);
				for(int i = 0; i < p; i++) {
					double Aip = gsl_matrix_get(A,i,p);
					double Aiq = gsl_matrix_get(A,i,q);
					gsl_matrix_set(A,i,p,c*Aip-s*Aiq);
					gsl_matrix_set(A,i,q,s*Aip+c*Aiq);
				}
				for(int i = p + 1; i < q; i++) {
					double Api = gsl_matrix_get(A,p,i);
					double Aiq = gsl_matrix_get(A,i,q);
					gsl_matrix_set(A,p,i,c*Api-s*Aiq);
					gsl_matrix_set(A,i,q,s*Api+c*Aiq);
				}
				for(int i = q + 1; i < n; i++) {
					double Api = gsl_matrix_get(A,p,i);
					double Aqi = gsl_matrix_get(A,q,i);
					gsl_matrix_set(A,p,i,c*Api-s*Aqi);
					gsl_matrix_set(A,q,i,s*Api+c*Aqi);
				}
				for(int i = 0; i < n; i++) {
					double Vip = gsl_matrix_get(V,i,p);
					double Viq = gsl_matrix_get(V,i,q);
					gsl_matrix_set(V,i,p,c*Vip-s*Viq);
					gsl_matrix_set(V,i,q,s*Vip+c*Viq);
				}
				
				
			}
		}
	}while(flag != 0);
	return iter;
}
void jacobi_value_by_value(gsl_matrix* A, gsl_vector* eig, int no_of_eigenvalues,int sort) {
	// set sort = +1 for normal sorting,
	// set sort = -1 for biggest eigenvalues
	// This immplementation is taken from Thomas Noerby's repository
	
	assert(sort == 1 || sort == -1);
	int n = A->size1, m = A->size2;
	assert(n == m);
	int n_eig = eig->size;
	assert(n_eig == no_of_eigenvalues);
	assert(no_of_eigenvalues <= n);

	int status = 0, iter = 0;
	double api, aqi, app, aqq, apq, phi, aip, aiq;
	double new_api, new_aqi, new_app, new_aqq, new_aip, new_aiq;
	double c, s, cc, ss, cs;

	if(no_of_eigenvalues == n) {
		no_of_eigenvalues = n-1; // the same number of eigenvalues, but prevents stuck
	}

	int p = 0, q, i;

	while(status == 0) {
		iter++;
		for(q = p+1; q<n; q++) {
			app = gsl_matrix_get(A,p,p);
			aqq = gsl_matrix_get(A,q,q);
			apq = gsl_matrix_get(A,p,q);
			phi = 1.0/2*atan2(sort*2.0*apq,sort*(aqq-app));

			c = cos(phi);
			s = sin(phi);
			cc = c*c;
			ss = s*s;
			cs = c*s;

			new_app = cc*app-2*cs*apq+ss*aqq;
			new_aqq = ss*app+2*cs*apq+cc*aqq;

			gsl_matrix_set(A,p,p,new_app);
			gsl_matrix_set(A,q,q,new_aqq);
			gsl_matrix_set(A,p,q,0.0);

			status = (app == new_app && aqq == new_aqq);
			if((status) && (p<no_of_eigenvalues-1)) {
				p++; status = 0; }
			for(i = 0; i<p;i++) {
				aip = gsl_matrix_get(A,i,p);
				aiq = gsl_matrix_get(A,i,q);

				new_aip = c*aip-s*aiq;
				new_aiq = s*aip+c*aiq;

				gsl_matrix_set(A,i,p,new_aip);
				gsl_matrix_set(A,i,q,new_aiq);
			}
			for(i = p+1; i<q;i++) {
				api = gsl_matrix_get(A,p,i);
				aiq = gsl_matrix_get(A,i,q);

				new_api = c*api-s*aiq;
				new_aiq = s*api+c*aiq;

				gsl_matrix_set(A,p,i,new_api);
				gsl_matrix_set(A,i,q,new_aiq);
			}
			for(i = q+1; i<m;i++) {
				api = gsl_matrix_get(A,p,i);
				aqi = gsl_matrix_get(A,q,i);

				new_api = c*api-s*aqi;
				new_aqi = s*api+c*aqi;

				gsl_matrix_set(A,p,i,new_api);
				gsl_matrix_set(A,q,i,new_aqi);
			}
		}
	}

	double aii;
	for(i = 0; i<n_eig; i++) {
		aii = gsl_matrix_get(A,i,i);
		gsl_vector_set(eig,i,aii);
	}

	printf("Converged after %d sweeps\n",iter);
}
void vector_print(gsl_vector* b)
{
	printf("(%li, 1) vector\n",b->size);
	int n = b->size;
	for(int i = 0; i<n; i++)
	{
		double bi = gsl_vector_get(b,i);
		if(fabs(bi) < 1e-12) bi = 0;
		printf("%7.7g ",bi);
	}
	printf("\n");
	printf("\n");
}
void matrix_print(gsl_matrix* A)
{
	int n = A->size1;
	int m = A->size2;	
	printf("(%i, %i) matrix\n",n,m);
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			double Aij = gsl_matrix_get(A,i,j);
			if(fabs(Aij) < 1e-12) Aij = 0;
			printf("%7.7g ",Aij);
		}
		printf("\n");
	}
	printf("\n");
}
int vector_equal(gsl_vector* a, gsl_vector* b,double eps)
{
	int equal = 1;
	int n = a->size;
	for(int i = 0; i<n; i++) {
		if(fabs(gsl_vector_get(a,i) - gsl_vector_get(b,i)) > eps) equal = 0;
	}
	return equal;
}
int matrix_equal(gsl_matrix* a, gsl_matrix* b,double eps)
{
	int equal = 1;
	int n = a->size1;
	for(int i = 0; i<n; i++)
	for(int j = 0; j<n; j++) {
		if(fabs(gsl_matrix_get(a,i,j) - gsl_matrix_get(b,i,j)) > eps) equal = 0;
	}
	return equal;
}
