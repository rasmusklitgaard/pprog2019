#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<math.h>
#include"myquad.h"
int main(void)
{
	int c_f = 0;
	long double f(long double x) { 
		c_f++; 
		return 4*sqrt( 1-pow((1-x),2) );
	}
	long double acc = 1e-16;
	long double eps = 1e-16;
	long double err;
	err = 0;
	
	long double real_pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;

	long double a = 0;
	long double b = 1;
	long double Q;
	Q = raq(f,a,b,acc,eps,&err);
	printf("\nUsing a copy of the project with long double instead of double\n");
	printf("Also using lower values of acc and eps\n");
	printf("acc = %Lg\neps = %Lg\n",acc,eps);
	printf("My estimate of pi\n100 digits of pi pi\n");
	printf("pi = %20.20Lg pm %10.10Lg\n",       Q,err);
	printf("pi = %20.20Lg\n", real_pi);
	printf("Used %d calls to user function\n\n",c_f);
	

		
	return 0;
}
