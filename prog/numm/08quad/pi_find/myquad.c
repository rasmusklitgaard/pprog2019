#include<math.h>
#include<assert.h>
#include"myquad.h"
long double raq24(		long double f(long double), 
			long double a,
			long double b,
			long double acc, 
			long double eps, 
			long double f2,
			long double f3,
			int recursiondepth,
			long double* err)
{
	assert(recursiondepth < 1e+6);
	long double f1 = f(a+(b-a)/6);
	long double f4 = f(a+5*(b-a)/6);
	long double Q = (2*f1+f2+f3+2*f4)/6 * (b-a);
	long double q = (f1+f2+f3+f4)/4*(b-a);
	long double tol = acc + eps*fabsl(Q);
	*err = fabsl(Q-q);
	if(*err < tol) {
		return Q;
	}
	else{
		long double err1;
		long double err2;
		long double Q1 = raq24(f,a,(a+b)/2,acc/sqrt(2.0),eps,f1,f2,recursiondepth+1,&err1);
		long double Q2 = raq24(f,(a+b)/2,b,acc/sqrt(2.0),eps,f3,f4,recursiondepth+1,&err2);
		*err = err1+err2;
		return Q1+Q2;
	}
	return Q;
}
long double raq(long double f(long double), long double a, long double b, long double acc, long double eps, long double* err)
{
	long double f2 = f(a+2*(b-a)/6);
	long double f3 = f(a+4*(b-a)/6);
	int recursiondepth = 0;
	return raq24(f,a,b,acc,eps,f2,f3,recursiondepth,err);
}
