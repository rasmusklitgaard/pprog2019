double raq24(		double f(double), 
			double a,
			double b,
			double acc, 
			double eps, 
			double f2,
			double f3,
			int recursiondepth,
			double* err);
double raq(double f(double), double a, double b, double acc, double eps, double* err);
double ccraq(double f(double), double acc, double eps, double* err);
