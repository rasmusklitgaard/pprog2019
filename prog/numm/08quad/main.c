#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<math.h>
#include"myquad.h" 
int main(void)
{
	int c_f1 = 0;
	int c_f2 = 0;
	int c_f3 = 0;
	int c_f4 = 0;
	int c_f5 = 0;
	int c_f6 = 0;
	double f1(double x) { 
		c_f1++; 
		return sqrt(x);
	}
	double f2(double x) { 
		c_f2++; 
		return 1/sqrt(x);
	}
	double f3(double x) { 
		c_f3++; 
		return log(x)/sqrt(x);
	}
	double f4(double x) { 
		c_f4++; 
		double res = pow(x,7)*(pow(x,3) * exp(-x) - sin(x))/(x-1);
		return res;
	}
	double f5(double x) {
		c_f5++;
		return 1/(x+1.0001);
	}
	double f6(double x) {
		c_f6++;
		return 1/(x+1.0001);
	}
	double acc = 1e-4;
	double eps = 1e-4;
	double err1,err2,err3,err4,err5,err6;
	err1 = err2 = err3 = err4 = err5 = err6 = 0;
	
	double a = 0;
	double b = 1;
	double a4 = 2;
	double b4 = 100;
	double Q1,Q2,Q3,Q4,Q5,Q6;//Q7,Q8;
	Q1 = raq(f1,a,b,acc,eps,&err1);
	Q2 = raq(f2,a,b,acc,eps,&err2);
	Q3 = raq(f3,a,b,acc,eps,&err3);
	Q4 = raq(f4,a4,b4,acc,eps,&err4);

	Q5 = ccraq(f5,acc,eps,&err5);
	Q6 = raq(f6,-1,1,acc,eps,&err6);
	

	printf("acc = %g\neps = %g\n",acc,eps);
	printf("integral of sqrt(x) from %g to %g = %g pm %g\n",       a,b,Q1,err1);
	printf("Used %d calls to user function\n\n",c_f1);
	printf("integral of 1/sqrt(x) from %g to %g = %g pm %g\n",     a,b,Q2,err2);
	printf("Used %d calls to user function\n\n",c_f2);
	printf("integral of ln(x)/sqrt(x) from %g to %g = %g pm %g \n",a,b,Q3,err3);
	printf("Used %d calls to user function\n\n",c_f3);
	printf("integral of x^7*(x^3*exp(-x)-sin(x))/(x-1) from %g to %g = %g pm %g \n",a4,b4,Q4,err4);
	printf("WolframAlpha says : 8.9*10^11\n");
	printf("Used %d calls to user function\n\n",c_f4);
	printf("\nClenshaw-Curtis:\n");
	printf("integral of 1/(x+1.0001) from -1 to 1 = %g pm %g\n",Q5,err5);
	printf("used %d calls to user function\n\n",c_f5);
	printf("Not Clenshaw-Curtis\n");
	printf("integral of 1/(x+1.0001) from -1 to 1 = %g pm %g\n",Q6,err6);
	printf("used %d calls to user function\n\n",c_f6);
	

		
	return 0;
}
