#include<math.h>
#include<assert.h>
#include"myquad.h"
double raq24(		double f(double), 
			double a,
			double b,
			double acc, 
			double eps, 
			double f2,
			double f3,
			int recursiondepth,
			double* err)
{
	assert(recursiondepth < 1e+6);
	double f1 = f(a+(b-a)/6);
	double f4 = f(a+5*(b-a)/6);
	double Q = (2*f1+f2+f3+2*f4)/6 * (b-a);
	double q = (f1+f2+f3+f4)/4*(b-a);
	double tol = acc + eps*fabs(Q);
	*err = fabs(Q-q);
	if(*err < tol) {
		return Q;
	}
	else{
		double err1;
		double err2;
		double Q1 = raq24(f,a,(a+b)/2,acc/sqrt(2.0),eps,f1,f2,recursiondepth+1,&err1);
		double Q2 = raq24(f,(a+b)/2,b,acc/sqrt(2.0),eps,f3,f4,recursiondepth+1,&err2);
		*err = sqrt(err1*err1+err2*err2);
		return Q1+Q2;
	}
	return Q;
}
double raq(double f(double), double a, double b, double acc, double eps, double* err)
{
	double f2 = f(a+2*(b-a)/6);
	double f3 = f(a+4*(b-a)/6);
	int recursiondepth = 0;
	return raq24(f,a,b,acc,eps,f2,f3,recursiondepth,err);
}
double ccraq(double f(double), double acc, double eps, double* err)
{
	double fn(double x) {
		return f(cos(x))*sin(x);
	}
	double a = 0;
	double b = M_PI;
	return raq(fn,a,b,acc,eps,err);
}
