#include"interp.h"
#include<stdio.h>
#include<math.h>
#define M_PI 3.14159265358979323846

struct qspline {int n; double *x, *y, *b, *c;};
typedef struct qspline qspline;
int main(void)
{
	int n = 15;
	double end = 6*M_PI;
	double step = end /n;
	double x[n];
	double y[n];
	for(int i = 0 ; i < n; i++) {
		double xnow = i * step;
		x[i] = xnow;
		y[i] = cos(xnow);
	}

	qspline* s = qspline_alloc(n, x, y);
	cspline* cs = cspline_alloc(n, x, y);
	double z0 = 0.1;
	double int_z0 = linterp_integ(n,x,y,z0);
	double linterp_start = linterp(n,x,y,x[0]);
	double qspline_start = qspline_evaluate(s,x[0]);
	double qint_start = qspline_integral(s,x[0]);
	double cspline_start = cspline_evaluate(cs,x[0]);
	double cint_start = cspline_integral(cs,x[0]);
	double qde_start = qspline_derivative(s,x[0]);
	double cde_start = cspline_derivative(cs,x[0]);

	for(int i = 0 ; i < n; i++) {
		fprintf(stderr,"%g %g %g %g %g %g %g %g %g %g %g\n"
							,x[i]
							,y[i]
							,z0
							,int_z0
							,linterp_start
							,qspline_start
							,qint_start
							,cspline_start
							,cint_start
							,qde_start
							,cde_start);
	}
	double zto = 2*M_PI;
	double zstep = 0.1;
	double lintp,integral,qspl,qint,cspl,cint,qde,cde;
	for (double z = z0; z < zto; z += zstep){
		integral = linterp_integ(n,x,y,z);
		lintp = linterp(n,x,y,z);
		qspl = qspline_evaluate(s,z);
		qint = qspline_integral(s,z);
		cspl = cspline_evaluate(cs,z);
		cint = cspline_integral(cs,z);
		qde = qspline_derivative(s,z);
		cde = cspline_derivative(cs,z);
		fprintf(stderr,"%g %g %g %g %g %g %g %g %g %g %g\n"
							,x[n-1]
							,y[n-1]
							,z
							,integral
							,lintp
							,qspl
							,qint
							,cspl
							,cint
							,qde
							,cde);
	}
	printf("Linear interpolation integral of cosine\n with %i points equaly spaced from 0 to pi= %g\n",n,integral);
	printf("quadratic spline integral from 0 to pi= %g\n",qint);
	printf("It should have been exact 0 .\n");

	qspline_free(s);
	cspline_free(cs);
	return 0;
}
