#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>
#include"interp.h"
struct qspline {int n; double *x, *y, *b, *c;};
 /* allocates and builds the quadratic spline */
qspline* qspline_alloc(int n, double *x, double *y)
{	
	qspline* s = (qspline*)malloc(sizeof(qspline)); 
	s->x= (double*) malloc(sizeof(double)*n);
	s->y= (double*) malloc(sizeof(double)*n);
	s->b= (double*) malloc(sizeof(double)*(n-1));
	s->c= (double*) malloc(sizeof(double)*(n-1));
	s->n = n;
	int i;
	//setting x and y
	for (i = 0; i < n; i++)
	{
		s->x[i] = x[i];
		s->y[i] = y[i];
	}
	//setting b and c -- c[0] = 0 -- need b and h
	double p[n-1], h[n-1];
	for (i = 0; i < n-1; i++) // h[i] is Delta x_i
	{
		h[i] = x[i+1] - x[i];
		p[i] = (y[i+1] - y[i]) / h[i];
	}
	s->c[0] = 0;
	for (i = 0; i < n-2; i++) // first c is set already
	{
		s->c[i+1] = 1/h[i+1] * (p[i+1] - p[i] - s->c[i] * h[i]);
	} 
	s->c[n-2] /= 2;
	for (i = n-3; i >= 0; i--)
	{
		s->c[i] = 1 / h[i] * (p[i+1] - p[i] - s->c[i+1]*h[i+1]);
	}
	for (i = 0; i < n-1; i++)
	{
		s->b[i] = p[i] - s->c[i]*h[i];
	}
	return s;
}
/* evaluates the prebuilt spline at point z */
double qspline_evaluate(struct qspline *s, double z)        
{
	assert(z>=s->x[0] && z<=s->x[s->n-1]);
	//need to do binary search to find relevant i.
	int i = 0, j = s->n-1;
	while(j - i > 1)
	{
		int mid = (i+j)/2;
		if(z > s->x[mid])
		{
			i = mid;
		} 
		else
		{
			j = mid;
		}
	}
	double h = z - s->x[i];
	double result = s->y[i] + h * (s->b[i] + h*s->c[i]); 
	return result;
}
 /* evaluates the derivative of the prebuilt spline at point z */
double qspline_derivative(struct qspline *s, double z)
{
	int i = 0, j = s->n;
	while(j - i > 1)
	{
		int mid = (i+j)/2;
		if(s->x[mid] >= z)
		{
			j = mid;
		} 
		if(s->x[mid] < z)
		{
			i = mid;
		}
	}
	double result = s->b[i] + 2 * s->c[i] * (z - s->x[i]);
	return result;
}
/* evaluates the integral of the prebuilt spline from x[0] to z */
double qspline_integral(struct qspline *s, double z)  
{
	int i = 0, j = s->n;
	while(j - i > 1)
	{
		int mid = (i+j)/2;
		if(s->x[mid] >= z)
		{
			j = mid;
		} 
		if(s->x[mid] < z)
		{
			i = mid;
		}
	}
	double intg = 0;
	double start,end,b,c,xi,yi;
	for (int k = 0; k < i; k++)
	{
		b = s->b[k];
		c = s->c[k];
		start = s->x[k]; 
		end = s->x[k+1];
		xi = s->x[k];
		yi = s->y[k];
	/*	intg += b*end*xi + b/2*(start*start - end*end) + c*end*xi*xi;
		intg += -1.0*c*end*end*xi + c/3*(pow(end,3) - pow(start,3));
		intg += -1.0*b*start*xi - c*start*xi*xi + c*start*start*xi;*/
		intg += -1.0*1/6*(start - end)*(3*b*(start + end - 2* xi) + 2*c*(start*start + start*end - 3*start*xi + end*end - 3*end*xi + 3*xi*xi) + 6*yi);
	}
	// now we need from x[i] to z.
	end = z;
	start = s->x[i];
	b = s->b[i];
	c = s->c[i];
	xi = s->x[i];
	yi = s->y[i];
	intg += -1.0*1/6*(start - end)*(3*b*(start + end - 2* xi) + 2*c*(start*start + start*end - 3*start*xi + end*end - 3*end*xi + 3*xi*xi) + 6*yi);
/*	intg += b*end*xi + b/2*(start*start - end*end) + c*end*xi*xi;
	intg += -1.0*c*end*end*xi + c/3*(pow(end,3) - pow(start,3));
	intg += -1.0*b*start*xi - c*start*xi*xi + c*start*start*xi;*/
	return intg;
}
 /* free memory allocated in qspline_alloc */
void qspline_free(struct qspline *s)
{
	free(s->x);
	free(s->y);
	free(s->b);
	free(s->c);
	free(s);
}
