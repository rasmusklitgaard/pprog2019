typedef struct qspline qspline;
typedef struct cspline cspline;

qspline * qspline_alloc(int n, double *x, double *y); 
double linterp(int n, double* x, double* y, double z);
double linterp_integ(int n, double* x, double* y, double z);
double qspline_evaluate(struct qspline *s, double z);        
double qspline_derivative(struct qspline *s, double z); 
double qspline_integral(struct qspline *s, double z);  
void qspline_free(struct qspline *s); 

cspline * cspline_alloc(int n, double *x, double *y); 
double cspline_evaluate(struct cspline *s, double z);        
double cspline_derivative(struct cspline *s, double z); 
double cspline_integral(struct cspline *s, double z);  
void cspline_free(cspline* s);
