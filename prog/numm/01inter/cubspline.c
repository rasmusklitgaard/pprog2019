#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<math.h>
#include"interp.h"
#define M_PI 3.14159265358979323846

struct cspline {int n; double *x, *y, *b, *c, *d;};

cspline* cspline_alloc(int n, double* x, double* y)
{
	cspline * s = (cspline*) malloc(sizeof(cspline)) ;
	s->n = n; 
	s->x = (double*) malloc (n* sizeof(double)) ;
	s->y = (double*) malloc (n* sizeof(double)) ;
	s->b = (double*) malloc (n* sizeof(double)) ;
	s->c = (double*) malloc ((n-1)* sizeof(double)) ;
	s->d = (double*) malloc ((n-1)* sizeof(double)) ;
	for(int i =0; i<n; i ++) {
		s->x[i] = x[i]; 
		s->y[i] = y[i]; 
	}
	double h[n-1];  
	double p[n-1];
	for(int i =0; i<n-1; i++) {
		h[i] = x[i+1] - x[i]; 
		assert(h[i]>0);
	}
	for(int i =0; i<n-1; i++) {
		p[i]=(y[i+1]-y[i]) / h[i];
	}
	double D[n], B[n], Q[n-1]; 
	D[0] = 2; 
	Q[0] = 1; 
	for (int i =0; i<n-2; i++) {
		B[i+1] = 3*(p[i] + p[i+1]*h[i] / h[i+1]);
		Q[i+1]=h[i] / h[i+1];
		D[i+1]=2*h[i] / h[i+1] + 2; 
	}
	D[n-1]=2;
	B[0] = 3*p[0]; 
	B[n-1] = 3*p[n-2]; 
	for (int i =1; i<n; i++) { 
		B[i] -= B[i-1] / D[i-1]; 
		D[i] -= Q[i-1] / D[i-1]; 
	}
	s->b[n-1] = B[n-1] / D[n-1];
	for (int i=n-2; i >=0;i --) {
		s->b[i] = (B[i] - Q[i] * s->b[i+1]) / D[i];
	}
	for (int i=0; i<n-1; i++){
		s->c[i]=(-2*s->b[i] - s->b[i+1] + 3*p[i]) / h[i];
		s->d[i]=( s->b[i] + s->b[i+1]-2*p[i]) / h[i] / h[i] ;
	}
	return s;
}
double cspline_evaluate(cspline* s, double z)
{
	int i = 0, j = s->n-1;
	while(j-i>1)
	{
		int mid = (i+j)/2;
		if(s->x[mid] > z){
			j = mid;
		}
		else{
			i = mid;
		}
	}
	// now we have i so we can calculate the spline.
	double xi = s->x[i], yi = s->y[i], bi = s->b[i], ci = s->c[i], di = s->d[i];
	double result = yi + bi * (z-xi) + ci * pow(z-xi,2) + di * pow(z-xi,3);
	return result;
}
double cspline_derivative(cspline* s, double z)
{
	int i = 0, j = s->n-1;
	while(j-i>1)
	{
		int mid = (i+j)/2;
		if(s->x[mid] > z){
			j = mid;
		}
		else{
			i = mid;
		}
	}
	double xi = s->x[i], bi = s->b[i], ci = s->c[i], di = s->d[i];
	double result = bi + 2*ci*(z-xi) + 3*di*pow(z-xi,2);
	return result;
}
double cspline_integral(cspline* s, double z)
{
	int i = 0, j = s->n-1;
	while(j-i>1)
	{
		int mid = (i+j)/2;
		if(s->x[mid] > z){
			j = mid;
		}
		else{
			i = mid;
		}
	}
	double intg = 0;
	for (int k = 0; k<i; k++)
	{
		double xi = s->x[k], xip = s->x[k+1];
		double yi = s->y[k], bi = s->b[k], ci = s->c[k], di = s->d[k];
		double hi = xip-xi;	
		double hi2 = xip*xip -xi*xi;
		double hi3 = pow(xip,3) - pow(xi,3);
		double hi4 = pow(xip,4) - pow(xi,4);
		intg += yi*hi + bi/2*hi2 - bi*xi*hi + ci/3*hi3 + ci*xi*xi*hi - ci*xi*hi2;
		intg += di/4*hi4 - di*pow(xi,3)*hi + di/2*xi*xi*hi2 - di*xi/3*hi3 + di*xi*xi*hi2;
		intg += -2.0/3*di*xi*hi3;
	}
	double xi = s->x[i], xip = z;
	double yi = s->y[i], bi = s->b[i], ci = s->c[i], di = s->d[i];
	double hi = xip-xi;	
	double hi2 = xip*xip -xi*xi;
	double hi3 = pow(xip,3) - pow(xi,3);
	double hi4 = pow(xip,4) - pow(xi,4);
	intg += yi*hi + bi/2*hi2 - bi*xi*hi + ci/3*hi3 + ci*xi*xi*hi - ci*xi*hi2;
	intg += di/4*hi4 - di*pow(xi,3)*hi + di/2*xi*xi*hi2 - di*xi/3*hi3 + di*xi*xi*hi2;
	intg += -2.0/3*di*xi*hi3;
	return intg;
}
void cspline_free(cspline* s)
{
	free(s->x);
	free(s->y);
	free(s->c);
	free(s->d);
	free(s->b);
	free(s);
}
