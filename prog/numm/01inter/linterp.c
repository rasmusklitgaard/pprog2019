#include<assert.h>
#include"interp.h"
#include<math.h>
#include<stdio.h>
double linterp(int n, double* x, double* y, double z)
{
	assert(n>1 && z>=x[0] && z<=x[n-1]);
	// find which interval we are in: do binary search
	int i = 0, j = n-1;
	while(j-i>1) {
		int mid = (j+i) / 2;
		if(z>x[mid]) {
			i = mid;
		}
		else{
			j = mid;
		}
	}
	double a = y[i];
	double b = (y[j] - y[i]) / (x[j] - x[i]);
	double result = a + b * (z - x[i]);
	return result;
}

double linterp_integ(int n, double* x, double* y, double z)
{
	// this is done by summing the integrals
	assert(n>1 && z>=x[0] && z<=x[n-1]);
	double sum = 0;
	double a_i,b_i;
	int k = 0,j=n-1;
	while(j-k>1) {
		int mid = (k+j) / 2;
		if(z>x[mid]) {
			k = mid;
		}
		else{
			j = mid;
		}
	}
	int i = 0;
	while(i < k) {
		// The integral between two points
		// I_i = a_i * x + 1/2 * b_i * x * x - b_i * x_i * x	
		a_i = y[i];
		b_i = (y[i+1] - y[i]) / (x[i+1] - x[i]);
		double integrand = b_i * 0.5 * (x[i+1]*x[i+1]-x[i]*x[i])   
					- b_i * (x[i+1]-x[i]) * x[i]
					+ a_i * (x[i+1]-x[i]);

		sum += integrand; 
	i++;
	}
	a_i = y[i];
	b_i = (y[i+1] - y[i]) / (x[i+1] - x[i]);

	sum += b_i * 0.5 * (z*z-x[i]*x[i])   
                    - b_i * (z-x[i]) * x[i]
                    + a_i * (z-x[i]);


	return sum;
}
