#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<math.h>
#include<stdio.h>
#include"lsf.h"
int main(void)
{
	int n = 9;
	int m = 3;
	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_matrix* cov = gsl_matrix_alloc(m,m);
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector* dy = gsl_vector_alloc(n);
	gsl_vector* b = gsl_vector_alloc(n);
	gsl_vector* c = gsl_vector_alloc(m);

	double xi,yi,dyi;
	int i = 0;
	while(scanf("%lg %lg %lg",&xi,&yi,&dyi) != EOF)
	{
		gsl_vector_set(x,i,xi);
		gsl_vector_set(b,i,yi/dyi);
		gsl_vector_set(dy,i,dyi);
		i++;
	}
	for(int i = 0; i<n; i++) {
		for(int j = 0; j < m; j++) {
			double xi  = gsl_vector_get(x, i);
			double dyi = gsl_vector_get(dy,i);
			gsl_matrix_set(A,i,j,funs(j,xi)/dyi);
		}
	}
	least_squares_fit(A,c,b,cov);

	for(xi = 0.1; xi < gsl_vector_get(x,n-1); xi += 0.1) {
		double funval = 0;
		double fvp = 0;
		double fvm = 0;
		for(int j = 0; j<m; j++) {
			double cj = gsl_vector_get(c,j);
			double sig_cj = pow(gsl_matrix_get(cov,j,j),0.5);
			funval += cj*funs(j,xi);
			fvp += (cj + sig_cj)*funs(j,xi);
			fvm += (cj - sig_cj)*funs(j,xi);
		}
		printf("%g %g %g %g\n",xi,funval,fvp,fvm);
	}

	gsl_matrix_free(A);
	gsl_matrix_free(cov);
	gsl_vector_free(b);
	gsl_vector_free(dy);
	gsl_vector_free(c);
	gsl_vector_free(x);
	return 0;
}
