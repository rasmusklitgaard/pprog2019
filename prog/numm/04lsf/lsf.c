#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<math.h>
#include"qr.h"

double funs(int i, double x)
{
	switch(i) {
		case 0: return log(x);
		case 1: return 1.0;
		case 2: return x;
		default: {
			fprintf(stderr, "funs: wrong ii: %d\n",i); return NAN;
		}
	}
}
void least_squares_fit(gsl_matrix* A, gsl_vector* c, gsl_vector* b, gsl_matrix* cov)
{
	int n = A->size1;
	int m = A->size2;
	gsl_matrix* R = gsl_matrix_alloc(m,m);
	gsl_matrix* B = gsl_matrix_alloc(m,m);
	qr_gs_decomp(A,R); // now A is Q
	qr_gs_solve(A,R,b,c);

	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,R,R,0.0,B);
	qr_gs_decomp(B,R); // B * R = old B
	qr_gs_inverse(B,R,cov);

	gsl_matrix_free(R);
	gsl_matrix_free(B);
} 

