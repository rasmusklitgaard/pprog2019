#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<assert.h>
#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<math.h>
#include"qr.h"
#include"root.h"
int main(void)
{
	int n0 = 2;
	gsl_vector* xeqs = gsl_vector_alloc(n0);
	gsl_vector* xros = gsl_vector_alloc(n0);
	gsl_vector* xhim = gsl_vector_alloc(n0);
	gsl_vector_set(xeqs,0,1.5);
	gsl_vector_set(xeqs,1,1);
	gsl_vector_set(xros,0,0.1);
	gsl_vector_set(xros,1,0.1);
	gsl_vector_set(xhim,0,3.1);
	gsl_vector_set(xhim,1,-1.1);
	double eps = 1e-8;
	int iter;	
	printf("Doing A.2 ) system of equations\n");
	printf("vector x at start:\n");
	vector_print(xeqs);	
	iter = newton_with_jacobian(f_eqs,xeqs,eps);
	printf("After finding the root: Should be about (0,9) or (9,0)\n");
	vector_print(xeqs);
	if(iter > 0) {
		printf("Converged after %i iterations\n",iter);
	} else {printf("Unable to converge\n");}

	printf("_________________________________________________________\n");
	printf("Doing A.3 ) Minimum of rosenbrock valley function\n");
	printf("vector x at start:\n");
	vector_print(xros);
	iter = newton_with_jacobian(f_ros,xros,eps);
	printf("After finding the minimum: Should be about (1,1)\n");
	vector_print(xros);
	if(iter > 0) {
		printf("Converged after %i iterations\n",iter);
	} else {printf("Unable to converge\n");}

	printf("_________________________________________________________\n");
	printf("Doing A.4 ) Minimum of Himmelblau function\n");
	printf("vector x at start:\n");
	vector_print(xhim);
	iter = newton_with_jacobian(f_him,xhim,eps);
	printf("After finding the minimum: Should be about (3,2), (-3.8,-3.3), (-2.8,3.1) or (3.6,-1.8)\n");
	vector_print(xhim);
	if(iter > 0) {
		printf("Converged after %i iterations\n",iter);
	} else {printf("Unable to converge\n");}

	printf("_________________________________________________________\n");
	printf("Doing the same calculations without jacobian\n");
	
	gsl_vector_set(xeqs,0,1.5);
	gsl_vector_set(xeqs,1,1.0);
	gsl_vector_set(xros,0,0.1);
	gsl_vector_set(xros,1,0.1);
	gsl_vector_set(xhim,0,3.1);
	gsl_vector_set(xhim,1,-1.1);
	double dx = sqrt(DBL_EPSILON);

	printf("Doing B.2 ) system of equations\n");
	printf("vector x at start:\n");
	vector_print(xeqs);	
	iter = newton(f_eqs_no_jac,xeqs,dx,eps);
	printf("After finding the root: Should be about (0,9) or (9,0)\n");
	vector_print(xeqs);
	if(iter > 0) {
		printf("Converged after %i iterations\n",iter);
	} else {printf("Unable to converge\n");}

	printf("_________________________________________________________\n");
	printf("Doing B.3 ) Minimum of rosenbrock valley function\n");
	printf("vector x at start:\n");
	vector_print(xros);
	iter = newton(f_ros_no_jac,xros,dx,eps);
	printf("After finding the minimum: Should be about (1,1)\n");
	vector_print(xros);
	if(iter > 0) {
		printf("Converged after %i iterations\n",iter);
	} else {printf("Unable to converge\n");}

	printf("_________________________________________________________\n");
	printf("Doing B.4 ) Minimum of Himmelblau function\n");
	printf("vector x at start:\n");
	vector_print(xhim);
	iter = newton(f_him_no_jac,xhim,dx,eps);
	printf("After finding the minimum: Should be about (3,2), (-3.8,-3.3), (-2.8,3.1) or (3.6,-1.8)\n");
	vector_print(xhim);
	if(iter > 0) {
		printf("Converged after %i iterations\n",iter);
	} else {printf("Unable to converge\n");}

	
	
	
	gsl_vector_free(xeqs);
	gsl_vector_free(xros);
	gsl_vector_free(xhim);
	return 0;
}

