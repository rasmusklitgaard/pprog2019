#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"qr.h"
#include"root.h"
#include<math.h>
#include<float.h>
#include<assert.h>
#include<stdio.h>
int newton_with_jacobian(
			void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J),
			gsl_vector* x,
			double epsilon)
{
	int n = x->size;
	int maxiter = 1000;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* J0 = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* deltax = gsl_vector_alloc(n);
	
	f(x, fx, J);
	double lambda;
	int iter = 0;
	do{
		iter++;
		qr_gs_decomp(J,R);
		gsl_vector_scale(fx,-1.0);
		qr_gs_solve(J,R,fx,deltax);
		gsl_vector_scale(fx,-1.0);
		
		lambda = 1;
		
		while(wclause(f,J,x,deltax,lambda)>(1-lambda*0.5)*calc_norm(fx)&&lambda>1.0/64)
		{lambda /= 2;}
		gsl_vector_scale(deltax,lambda);
		gsl_vector_add(x,deltax);
		gsl_vector_scale(deltax,1.0/lambda);

		f(x,fx,J);
		if(iter > maxiter) {
			printf("Unable to converge after. Maxiter = %i\n",maxiter);
			iter = -1;
			break;
		}
	} while(epsilon < calc_norm(fx)); 

	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(deltax);
	return iter;
}
int newton(
	void f(const gsl_vector* x, gsl_vector* fx),
	gsl_vector* x,
	double dx,
	double epsilon)
{
	int maxiter = 1000;
	int n = x->size;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* Jcopy = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* deltax = gsl_vector_alloc(n);
	gsl_vector* Jdx = gsl_vector_alloc(n);
	
	gsl_matrix_set_zero(J);
	// First calculating the jacobian in point x.
	f(x,fx);
	calc_jacobian(f,x,fx,J,dx); // now we have start jacobian and can use it again later. 
	f(x, fx);
	int iter = 0;
	double lambda;
	do{
		iter++;
		gsl_matrix_memcpy(Jcopy,J);
		qr_gs_decomp(J,R);
		gsl_vector_scale(fx,-1.0);
		qr_gs_solve(J,R,fx,deltax);
		gsl_vector_scale(fx,-1.0);

		gsl_matrix_memcpy(J,Jcopy); //restoring J:

		lambda = 1;
		while(wclause_no_jac(f,J,x,deltax,lambda)>(1-lambda*0.5)*calc_norm(fx)
			&&lambda>1.0/64) lambda /= 2;

		gsl_vector_scale(deltax,lambda);
		gsl_vector_add(x,deltax);
		gsl_vector_scale(deltax,1.0/lambda);

		f(x,fx);
		calc_jacobian(f,x,fx,J,dx); 

		if(iter > maxiter) {
			printf("Unable to converge after. Maxiter = %i\n",maxiter);
			iter = -1;
			break;
		}
	} while(epsilon < calc_norm(fx));

	gsl_matrix_free(J);
	gsl_matrix_free(Jcopy);
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(deltax);
	gsl_vector_free(Jdx);
	return iter;
}
void calc_jacobian(void f(const gsl_vector* x, gsl_vector* fx),
		 	gsl_vector* x,
			gsl_vector* fx,
			gsl_matrix* J,
			double dx)
{
	int n = x->size;
	gsl_vector* fx0 = gsl_vector_alloc(n);
	gsl_vector* deltax = gsl_vector_alloc(n);
	gsl_vector_set_zero(deltax);
	gsl_vector_memcpy(fx0,fx);
	for (int i = 0; i<n; i++) {
		for(int j = 0;j<n; j++) {
			gsl_vector_set(deltax,j,dx);
			gsl_vector_add(x,deltax);
			f(x,fx); // fx = f(x0,x1...,xj+dx,xj+1,..,xn)
			gsl_vector_sub(x,deltax);
			gsl_vector_set(deltax,j,0.0);
			double fx0i = gsl_vector_get(fx0,i);
			double fxi = gsl_vector_get(fx,i);
			gsl_matrix_set(J,i,j,(fxi-fx0i)/dx);
		}
	} 
	free(fx0);
	free(deltax);
}
double calc_norm(gsl_vector* v)
{
	double sum = 0;
	int n = v->size;
	for(int i = 0; i<n; i++) {
		double vi = gsl_vector_get(v,i);
		sum += vi*vi;
	}
	return sqrt(sum);
	
}
double wclause(   void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J)
		, gsl_matrix* J
		, gsl_vector* x
		, gsl_vector* deltax
		, double lambda) //size of norm(f(x+lambda*deltax))
{
	gsl_vector* fx = gsl_vector_alloc(x->size);
	gsl_vector_scale(deltax,lambda);
	gsl_vector_add(x,deltax);
	f(x,fx,J);
	double norm = calc_norm(fx);
	gsl_vector_sub(x,deltax);
	gsl_vector_scale(deltax,1.0/lambda);
	gsl_vector_free(fx);
	return norm;
}
double wclause_no_jac(   void f(const gsl_vector* x, gsl_vector* fx)
		, gsl_matrix* J
		, gsl_vector* x
		, gsl_vector* deltax
		, double lambda) //size of norm(f(x+lambda*deltax))
{
	gsl_vector* fx = gsl_vector_alloc(x->size);
	gsl_vector_scale(deltax,lambda);
	gsl_vector_add(x,deltax);
	f(x,fx);
	double norm = calc_norm(fx);
	gsl_vector_sub(x,deltax);
	gsl_vector_scale(deltax,1.0/lambda);
	gsl_vector_free(fx);
	return norm;
}
void f_eqs(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J) 
{
	assert(x->size == 2);
	int n = x->size;
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double A = 10000;
	double fx0 = A*x0*x1-1;
	double fx1 = exp(-x0) + exp(-x1) -1-1/A;
	gsl_vector_set(fx,0,fx0);
	gsl_vector_set(fx,1,fx1);
	double J00 = A*x1;
	double J01 = A*x0;
	double J10 = -exp(-x0);
	double J11 = -exp(-x1);
	gsl_matrix_set(J,0,0,J00);
	gsl_matrix_set(J,0,1,J01);
	gsl_matrix_set(J,1,0,J10);
	gsl_matrix_set(J,1,1,J11);
}
void f_ros(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J)
{
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double fx0 = 2*(x0-1) - 400*(x0*x1 - pow(x0,3));
	double fx1 = 200*(x1-x0*x0);
	gsl_vector_set(fx,0,fx0);
	gsl_vector_set(fx,1,fx1);
	double J00 = 2-400*x1+1200*x0*x0;
	double J01 = -400*x0;
	double J10 = -400*x0;
	double J11 = 200;
	gsl_matrix_set(J,0,0,J00);
	gsl_matrix_set(J,0,1,J01);
	gsl_matrix_set(J,1,0,J10);
	gsl_matrix_set(J,1,1,J11);
}
void f_him(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J)
{
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double fx0 = 4*pow(x0,3)+4*x0*x1-42*x0+2*x1*x1-14;
	double fx1 = 2*x1+2*x0*x0-22+4*pow(x1,3)+4*x0*x1-28*x1;
	gsl_vector_set(fx,0,fx0);
	gsl_vector_set(fx,1,fx1);
	double J00 = 12*x0*x0+4*x1-42;
	double J01 = 4*x0+4*x1;
	double J10 = 4*x0+4*x1;
	double J11 = 2+12*x1*x1+4*x0-28;
	gsl_matrix_set(J,0,0,J00);
	gsl_matrix_set(J,0,1,J01);
	gsl_matrix_set(J,1,0,J10);
	gsl_matrix_set(J,1,1,J11);
}
void f_eqs_no_jac(const gsl_vector* x, gsl_vector* fx) 
{
	assert(x->size == 2);
	int n = x->size;
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double A = 10000;
	double fx0 = A*x0*x1-1;
	double fx1 = exp(-x0) + exp(-x1) -1-1/A;
	gsl_vector_set(fx,0,fx0);
	gsl_vector_set(fx,1,fx1);
}
void f_ros_no_jac(const gsl_vector* x, gsl_vector* fx)
{
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double fx0 = 2*(x0-1) - 400*x0*(x1 - x0*x0);
	double fx1 = 200*(x1-x0*x0);
	gsl_vector_set(fx,0,fx0);
	gsl_vector_set(fx,1,fx1);
}
void f_him_no_jac(const gsl_vector* x, gsl_vector* fx)
{
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double fx0 = 4*pow(x0,3)+4*x0*x1-42*x0+2*x1*x1-14;
	double fx1 = 2*x1+2*x0*x0-22+4*pow(x1,3)+4*x0*x1-28*x1;
	gsl_vector_set(fx,0,fx0);
	gsl_vector_set(fx,1,fx1);
}
void vector_print(gsl_vector* b)
{
	printf("(%li, 1) vector\n",b->size);
	int n = b->size;
	for(int i = 0; i<n; i++)
	{
		double bi = gsl_vector_get(b,i);
		if(fabs(bi) < 1e-12) bi = 0;
		printf("%7.7g ",bi);
	}
	printf("\n");
	printf("\n");
}
void matrix_print(gsl_matrix* A)
{
	int n = A->size1;
	int m = A->size2;	
	printf("(%i, %i) matrix\n",n,m);
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			double Aij = gsl_matrix_get(A,i,j);
			if(fabs(Aij) < 1e-12) Aij = 0;
			printf("%7.7g ",Aij);
		}
		printf("\n");
	}
	printf("\n");
}
int vector_equal(gsl_vector* a, gsl_vector* b,double eps)
{
	int equal = 1;
	int n = a->size;
	for(int i = 0; i<n; i++) {
		if(fabs(gsl_vector_get(a,i) - gsl_vector_get(b,i)) > eps) equal = 0;
	}
	return equal;
}
int matrix_equal(gsl_matrix* a, gsl_matrix* b,double eps)
{
	int equal = 1;
	int n = a->size1;
	for(int i = 0; i<n; i++)
	for(int j = 0; j<n; j++) {
		if(fabs(gsl_matrix_get(a,i,j) - gsl_matrix_get(b,i,j)) > eps) equal = 0;
	}
	return equal;
}
