double calc_norm(gsl_vector* v);
void calc_jacobian(void f(const gsl_vector* x, gsl_vector* fx),
		 	gsl_vector* x,
			gsl_vector* fx,
			gsl_matrix* J,
			double dx);
int newton_with_jacobian(void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J)
			  , gsl_vector* x
			  , double epsilon);
int newton(
	void f(const gsl_vector* x, gsl_vector* fx),
	gsl_vector* x,
	double dx,
	double epsilon);
double wclause(   void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J)
		, gsl_matrix* J
		, gsl_vector* x
		, gsl_vector* deltax
		, double lambda); 
double wclause_no_jac(   void f(const gsl_vector* x, gsl_vector* fx)
		, gsl_matrix* J
		, gsl_vector* x
		, gsl_vector* deltax
		, double lambda);
void f_eqs(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);
void f_ros(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);
void f_him(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);
void f_eqs_no_jac(const gsl_vector* x, gsl_vector* fx) ;
void f_ros_no_jac(const gsl_vector* x, gsl_vector* fx);
void f_him_no_jac(const gsl_vector* x, gsl_vector* fx);
int matrix_equal(gsl_matrix* a, gsl_matrix* b,double eps);
int vector_equal(gsl_vector* a, gsl_vector* b,double eps);
void matrix_print(gsl_matrix* A);
void vector_print(gsl_vector* b);
