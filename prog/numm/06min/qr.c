#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include"qr.h"
void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R)
{
	int n = A->size1; int m = A->size2;
	for(int i = 0; i < m; i++)
	{	
		//calculate inner product ai * ai
		double inp = 0;
		for(int j = 0; j < n; j++)
		{
			double aji = gsl_matrix_get(A,j,i);
			inp += pow(aji,2); 
		}
		double Rii = sqrt(inp);
		gsl_matrix_set(R,i,i,Rii);
		//calculate qi = ai / Rii
		for(int j = 0; j < n; j++)
		{
			double aji = gsl_matrix_get(A,j,i);
			gsl_matrix_set(A,j,i,aji/Rii);
		}

		for(int j = i+1; j < m; j++)
		{
			//calculate inner product Rij = q_i * a_j
			double inp_ij = 0;
			for(int k = 0; k < n; k++)
			{
				double qki = gsl_matrix_get(A,k,i);
				double akj = gsl_matrix_get(A,k,j);
				inp_ij += qki * akj;
			}
			gsl_matrix_set(R,i,j,inp_ij);
			//calculate aj = aj - qi * Rij
			for(int k = 0; k < n; k++)
			{
				double akj = gsl_matrix_get(A,k,j);
				double qki = gsl_matrix_get(A,k,i);
				double Rij = gsl_matrix_get(R,i,j);
				gsl_matrix_set(A,k,j,akj-qki*Rij);
			}
			
		}
	}
}
void backsub(const gsl_matrix* U, gsl_vector *c)
{
	// taken from lineq.pdf lecture notes
	int n = c->size;
	for(int i = n-1; i>=0; i--) {
		double s = gsl_vector_get(c,i);
		for(int k = i+1; k<n; k++) {
			s -= gsl_matrix_get(U,i,k) * gsl_vector_get(c,k);
		}
		gsl_vector_set(c,i,s/gsl_matrix_get(U,i,i));
	}
}
void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x)
{
	gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x);
/*	for(int i = 0; i < Q->size2; i++) {
		double sum = 0;
		for(int j = 0; j < Q->size1; j++) {
			sum += gsl_matrix_get(Q,j,i) * gsl_vector_get(b,j);
		}
		gsl_vector_set(x,i,sum);
	}
*/
	backsub(R,x);	
}
void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B)
{
	int m = Q->size2;
	gsl_matrix_set_identity(B);
	gsl_vector* ej_c = gsl_vector_alloc(Q->size1);
	gsl_vector_set_zero(ej_c);
	for(int j = 0; j < m; j++) {
		gsl_vector_view ej = gsl_matrix_column(B,j);
		gsl_vector_set(ej_c,j,1);
		qr_gs_solve(Q,R,(const gsl_vector*) ej_c,&ej.vector);
		gsl_vector_set(ej_c,j,0);
	}
	free(ej_c);
}
