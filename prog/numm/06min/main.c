#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include"min.h"
int main(void)
{
	int n = 2;
	int ndat = 10;
	int nfit = 3;

	gsl_vector* fit_params = gsl_vector_alloc(nfit);

	gsl_vector* xros = gsl_vector_alloc(n);
	gsl_vector* xhim = gsl_vector_alloc(n);

	gsl_vector* xrosb = gsl_vector_alloc(n);
	gsl_vector* xhimb = gsl_vector_alloc(n);

	gsl_vector* xdat = gsl_vector_alloc(ndat);
	gsl_vector* ydat = gsl_vector_alloc(ndat); 
	gsl_vector* edat = gsl_vector_alloc(ndat); 
	
	gsl_vector_set(xros,0,-3);
	gsl_vector_set(xros,1,-10);
	gsl_vector_set(xhim,0,10);
	gsl_vector_set(xhim,1,20);

	double eps = 1e-6;
	int iter;
	int iterb;
	
	printf("___________________________________________________________\n");
	printf("Doing A: himmelblau. Start point x:\n");
	vector_print(xhim);
	iter = newton_min(f_him,xhim,eps);
	printf("Converged after %i iterations\n",iter);
	printf("Should be about (3,2), (-3.8,-3.3), (-2.8,3.1) or (3.6,-1.8)\n");
	printf("Converged to x:\n");
	vector_print(xhim);

	printf("___________________________________________________________\n");
	printf("Doing A: rosenbrock. Start point x:\n");
	vector_print(xros);
	iter = newton_min(f_ros,xros,eps);
	printf("Converged after %i iterations\n",iter);
	printf("Should be about (1,1)\n");
	printf("Converged to x:\n");
	vector_print(xros);

	printf("___________________________________________________________\n");
	printf("Doing B: Broydens update\n");
	printf("Doing B: himmelblau. Start point x:\n");
	vector_print(xhimb);
	iterb = quasi_newton_broyden(f_himb, xhimb,eps);
	printf("Converged after %i iterations\n",iterb);
	printf("Newton method with Hessian converged after %i iterations\n",iterb);
	printf("Should be about (3,2), (-3.8,-3.3), (-2.8,3.1) or (3.6,-1.8)\n");
	printf("Converged to x:\n");
	vector_print(xhimb);

	printf("___________________________________________________________\n");
	printf("Doing B: rosenbrock. Start point x:\n");
	vector_print(xrosb);
	iterb = quasi_newton_broyden(f_rosb, xrosb,eps);
	printf("Converged after %i iterations\n",iterb);
	printf("Newton method with Hessian converged after %i iterations\n",iterb);
	printf("Should be about (1,1)\n");
	printf("Converged to x:\n");
	vector_print(xrosb);

	printf("___________________________________________________________\n");
	printf("Doing B: Fit to data\n");
	double tarr[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double yarr[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double earr[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	for(int i = 0; i<ndat;i++) {
		gsl_vector_set(xdat,i,tarr[i]);
		gsl_vector_set(ydat,i,yarr[i]);
		gsl_vector_set(edat,i,earr[i]);
	}

	gsl_vector_set_all(fit_params,1);
	
	printf("Fitting parameters start guess:\n");
	vector_print(fit_params);

	

	double decay(double x, double A,double T,double B) { return A*exp(-x/T)+B; }
	double fit_func(gsl_vector* x, gsl_vector* df)
	{
		double A = gsl_vector_get(x,0);
		double T = gsl_vector_get(x,1);
		double B = gsl_vector_get(x,2);

		double f(double x) {return A*exp(-x/T)+B;}

		double sum = 0;
		double sumA = 0;
		double sumT = 0;
		double sumB = 0;
		for(int i = 0; i<ndat; i++) {
			double ti = gsl_vector_get(xdat,i);
			double yi = gsl_vector_get(ydat,i);
			double ei = gsl_vector_get(edat,i);
			sum  += pow( (f(ti) - yi) ,2)/pow(ei,2);
			sumA += (2*A*exp(-2*ti/T) + 2*B*exp(-ti/T) - 2*yi*exp(-ti/T))/ei/ei;
			sumT += (A*A*exp(-2*ti/T)*2*ti/T/T
				+ 2*B*A*exp(-ti/T)*ti/T/T
				- 2*yi*A*exp(-ti/T)*ti/T/T)/ei/ei;
			sumB += (2*B + 2*A*exp(-ti/T) - 2*yi)/ei/ei;
		}
		gsl_vector_set(df,0,sumA);
		gsl_vector_set(df,1,sumT);
		gsl_vector_set(df,2,sumB);
		
		return sum;
	}
	int iter_fit = quasi_newton_broyden(fit_func,fit_params,eps);

	printf("Converged after %i iterations\n",iter_fit);
	printf("Converged to x:\n");
	vector_print(fit_params);
	printf("Lifetime T = %g in the same units as the given times.\n",gsl_vector_get(fit_params,1));
	
	for(double time = 0; time < tarr[ndat-1]; time += 0.1) {
		double A = gsl_vector_get(fit_params,0);
		double T = gsl_vector_get(fit_params,1);
		double B = gsl_vector_get(fit_params,2);
		fprintf(stderr, "%g %g\n",time,decay(time,A,T,B));
	}

	gsl_vector_free(xros);
	gsl_vector_free(xhim);
	gsl_vector_free(xrosb);
	gsl_vector_free(xhimb);
	gsl_vector_free(xdat);
	gsl_vector_free(ydat);
	gsl_vector_free(edat);
	return 0;
}
