int newton_min ( double f(gsl_vector* point, gsl_vector* df, gsl_matrix* H),
		gsl_vector* x,
		double eps);
int quasi_newton_broyden(double f(gsl_vector* point, gsl_vector* df),
			gsl_vector* x,
			double eps);
double calc_rs( double f(gsl_vector* point, gsl_vector* df, gsl_matrix* H),
		gsl_vector* x,
		gsl_vector* deltax,
		double lambda,
		double alpha);
double calc_ls( double f(gsl_vector* point, gsl_vector* df, gsl_matrix* H),
		gsl_vector* x,
		gsl_vector* deltax,
		double lambda);
double calc_norm(gsl_vector* v);
double f_ros(gsl_vector* x, gsl_vector* df, gsl_matrix* H);
double f_rosb(gsl_vector* x, gsl_vector* df);
double f_him(gsl_vector* x, gsl_vector* df, gsl_matrix* H);
double f_himb(gsl_vector* x, gsl_vector* df);
void vector_print(gsl_vector* b);
void matrix_print(gsl_matrix* A);
int vector_equal(gsl_vector* a, gsl_vector* b,double eps);
int matrix_equal(gsl_matrix* a, gsl_matrix* b,double eps);
double dot_product(double alpha, gsl_vector* a, gsl_vector* b);
