#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>
#include<assert.h>
#include"qr.h"
#include"min.h"
int newton_min ( double f(gsl_vector* point, gsl_vector* df, gsl_matrix* H),
		gsl_vector* x,
		double eps)
{
	int n = x->size;
	gsl_matrix* H  = gsl_matrix_alloc(n,n);
	gsl_matrix* R  = gsl_matrix_alloc(n,n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* deltax = gsl_vector_alloc(n);
	double lambda, alpha;
	alpha = 1e-2;
	int iter = 0;
	f(x,df,H);
	do{
		iter++;
		qr_gs_decomp(H,R);
		gsl_vector_scale(df,-1.0);
		qr_gs_solve(H,R,df,deltax);
		gsl_vector_scale(df,-1.0);
		lambda = 1;
		while ( ( calc_ls(f,x,deltax,lambda) >= calc_rs(f,x,deltax,lambda,alpha) ) 
			&& (lambda > 1.0/64)) 
		{
			lambda /= 2;
		}
		gsl_vector_scale(deltax,lambda);
		gsl_vector_add(x,deltax);
		gsl_vector_scale(deltax,1.0/lambda);
		f(x,df,H);
	} while(calc_norm(df) >= eps && iter < 50000);
	gsl_matrix_free(H);
	gsl_matrix_free(R);
	gsl_vector_free(df);
	gsl_vector_free(deltax);
	return iter;
}
double calc_rs( double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
		gsl_vector* x,
		gsl_vector* deltax,
		double lambda,
		double alpha)
{
	int n = x->size;
	double rv, sum;
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_matrix* H  = gsl_matrix_alloc(n,n);
	sum = f(x,df,H);
	gsl_vector_scale(deltax, lambda);
	gsl_blas_ddot(deltax, df, &rv);
	gsl_vector_scale(deltax, 1.0/lambda);
	rv = alpha*rv + sum;
	gsl_vector_free(df);
	gsl_matrix_free(H);
	return rv;
}
double calc_ls( double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
		gsl_vector* x,
		gsl_vector* deltax,
		double lambda)
{
	int n = x->size;
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_matrix* H  = gsl_matrix_alloc(n,n);
	gsl_vector_scale(deltax,lambda);
	gsl_vector_add(x,deltax);
	double rv = f(x,df,H);
	gsl_vector_sub(x,deltax);
	gsl_vector_scale(deltax,1.0/lambda);
	gsl_vector_free(df);
	gsl_matrix_free(H);
	return rv;
}
int quasi_newton_broyden(double f(gsl_vector* point, gsl_vector* df),
			gsl_vector* xstart,
			double eps)
{
	// This code is kindly borrowed from Thomas Noerby
	int n = xstart->size;
	gsl_matrix* B = gsl_matrix_alloc(n,n);
	gsl_vector* df = gsl_vector_alloc(n); 
	gsl_vector* old_df = gsl_vector_alloc(n); 
	gsl_vector* x = gsl_vector_alloc(n); 
	gsl_vector* delta_x = gsl_vector_alloc(n); 
	gsl_vector* s = gsl_vector_alloc(n); 
	gsl_vector* y = gsl_vector_alloc(n);	
	gsl_vector* u = gsl_vector_alloc(n);
	gsl_vector* c = gsl_vector_alloc(n);
	gsl_matrix* delta_B = gsl_matrix_alloc(n,n);
	gsl_vector_memcpy(x,xstart);
	
	gsl_matrix_set_identity(B);

	int iter = 0; 
	int iter_func = 0;
	double length; 
	double lambda; 
	double alpha = 1e-3; 
	double func_val;
	double cur_func_val;
	double armijo;
	double sum, bij, pj, yj, si, sty, ui, dfsi, dfi, ci, sj, dbij;
	int i, j;

	do {
		iter++;
		func_val = f(x,df);
		iter_func++;
		gsl_vector_memcpy(old_df,df);

		// delta_x
		sum = 0;
		for(i = 0; i<n; i++) {
			for(j = 0; j<n; j++) {
				bij = gsl_matrix_get(B,i,j);		
				pj = gsl_vector_get(df,j);
				sum -= bij*pj;
			}
			gsl_vector_set(delta_x,i,sum);
			sum = 0;
		}
		
		lambda = 2;
		do {
			lambda/= 2;
			gsl_vector_set_zero(s);
			gsl_blas_daxpy(lambda,delta_x,s);
			gsl_vector_memcpy(xstart,s);
			gsl_vector_add(xstart,x);
			cur_func_val = f(xstart,df); // df = delta_phi (x+s)
			iter_func++;
			armijo = func_val+dot_product(alpha,s,old_df);
		}
		while(cur_func_val >= armijo && lambda > 1.0/pow(2,14));
		if(lambda == 1.0/pow(2,14)) { // from Jens Jensen
			gsl_matrix_set_identity(B);
		}
		gsl_vector_add(x,s); // x+s

		// y
		for(i = 0; i<n; i++) {
			dfsi = gsl_vector_get(df,i);
			dfi = gsl_vector_get(old_df,i);
			gsl_vector_set(y,i,dfsi-dfi);
		}

		// u	
		for(i = 0; i<n; i++) {
			for(j = 0; j<n; j++) {
				bij = gsl_matrix_get(B,i,j);
				yj = gsl_vector_get(y,j);
				sum -= bij*yj;
			}
			si = gsl_vector_get(s,i);
			sum += si;
			gsl_vector_set(u,i,sum);
			sum = 0;
		}
		
		// c
		sty = dot_product(1.0,s,y);
		if(sty > 1e-6) {
			for(i = 0; i<n; i++) {
				ui = gsl_vector_get(u,i);		
				ui/=sty;
				gsl_vector_set(c,i,ui);
			}
		}

		// delta_B
		for(i = 0; i<n; i++) {
			ci = gsl_vector_get(c,i);
			for(j = 0; j<n; j++) {
				sj = gsl_vector_get(s,j);
				gsl_matrix_set(delta_B,i,j,ci*sj);
			}
		}

		// new B
		for(i = 0; i<n; i++) {
			for(j = 0; j<n; j++) {
				bij = gsl_matrix_get(B,i,j);
				dbij = gsl_matrix_get(delta_B,i,j);
				gsl_matrix_set(B,i,j,bij+dbij);
			}
		}
		
		length = calc_norm(df);
	}
	while(length >= eps);

	gsl_vector_memcpy(xstart,x);
	/*
	printf("Gradient\n");
	vector_print(df);
	printf("The function converged after %d iterations and %d function calls\n",iter,iter_func);
	*/

	gsl_matrix_free(B);
	gsl_vector_free(df); 
	gsl_vector_free(old_df); 
	gsl_vector_free(x); 
	gsl_vector_free(delta_x); 
	gsl_vector_free(s); 
	gsl_vector_free(y);	
	gsl_vector_free(u);
	gsl_vector_free(c);
	gsl_matrix_free(delta_B);
	return iter;

}
double calc_norm(gsl_vector* v)
{
	double sum = 0;
	int n = v->size;
	for(int i = 0; i<n; i++) {
		double vi = gsl_vector_get(v,i);
		sum += vi*vi;
	}
	return sqrt(sum);
	
}
double f_ros(gsl_vector* x, gsl_vector* df, gsl_matrix* H)
{
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double H00 = 2 - 400*x1 + 1200*x0*x0;
	double H01 = -400 * x0;
	double H10 = -400 * x0;
	double H11 = 200;
	double df0 = 2*(x0-1) - 400*x0*(x1-x0*x0);
	double df1 = 200*x1 - 200*x0*x0;
	gsl_vector_set(df,0,df0);	
	gsl_vector_set(df,1,df1);	
	gsl_matrix_set(H,0,0,H00);
	gsl_matrix_set(H,0,1,H01);
	gsl_matrix_set(H,1,0,H10);
	gsl_matrix_set(H,1,1,H11);
	return pow((1-x0),2) + 100 * pow((x1-x0*x0),2);
}
double f_rosb(gsl_vector* x, gsl_vector* df)
{
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double df0 = 2*(x0-1) - 400*x0*(x1-x0*x0);
	double df1 = 200*x1 - 200*x0*x0;
	gsl_vector_set(df,0,df0);	
	gsl_vector_set(df,1,df1);	
	return pow((1-x0),2) + 100 * pow((x1-x0*x0),2);
}
double f_him(gsl_vector* x, gsl_vector* df, gsl_matrix* H)
{
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double H00 = 8*x0*x0 + 4*(x0*x0+x1-11) + 2;
	double H01 = 4*x0 + 4*x1;
	double H10 = 4*x0 + 4*x1;
	double H11 = 2 + 8*x1*x1 + 4*(x0+x1*x1-7);
	double df0 = 4*x0*(x0*x0+x1-11) + 2*(x0+x1*x1-7);
	double df1 = 2*(x0*x0+x1-11) + 4*x1*(x0+x1*x1-7);
	gsl_vector_set(df,0,df0);	
	gsl_vector_set(df,1,df1);	
	gsl_matrix_set(H,0,0,H00);
	gsl_matrix_set(H,0,1,H01);
	gsl_matrix_set(H,1,0,H10);
	gsl_matrix_set(H,1,1,H11);
	return pow(x0*x0 + x1 - 11,2) + pow(x0+x1*x1-7,2);
}
double f_himb(gsl_vector* x, gsl_vector* df)
{
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double df0 = 4*x0*(x0*x0+x1-11) + 2*(x0+x1*x1-7);
	double df1 = 2*(x0*x0+x1-11) + 4*x1*(x0+x1*x1-7);
	gsl_vector_set(df,0,df0);	
	gsl_vector_set(df,1,df1);	
	return pow(x0*x0 + x1 - 11,2) + pow(x0+x1*x1-7,2);
}
void vector_print(gsl_vector* b)
{
	printf("(%li, 1) vector\n",b->size);
	int n = b->size;
	for(int i = 0; i<n; i++)
	{
		double bi = gsl_vector_get(b,i);
		if(fabs(bi) < 1e-12) bi = 0;
		printf("%7.7g ",bi);
	}
	printf("\n");
	printf("\n");
}
void matrix_print(gsl_matrix* A)
{
	int n = A->size1;
	int m = A->size2;	
	printf("(%i, %i) matrix\n",n,m);
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			double Aij = gsl_matrix_get(A,i,j);
			if(fabs(Aij) < 1e-12) Aij = 0;
			printf("%7.7g ",Aij);
		}
		printf("\n");
	}
	printf("\n");
}
int vector_equal(gsl_vector* a, gsl_vector* b,double eps)
{
	int equal = 1;
	int n = a->size;
	for(int i = 0; i<n; i++) {
		if(fabs(gsl_vector_get(a,i) - gsl_vector_get(b,i)) > eps) equal = 0;
	}
	return equal;
}
int matrix_equal(gsl_matrix* a, gsl_matrix* b,double eps)
{
	int equal = 1;
	int n = a->size1;
	for(int i = 0; i<n; i++)
	for(int j = 0; j<n; j++) {
		if(fabs(gsl_matrix_get(a,i,j) - gsl_matrix_get(b,i,j)) > eps) equal = 0;
	}
	return equal;
}
double dot_product(double alpha, gsl_vector* a, gsl_vector* b)
{
	int n = a->size;
	int m = b->size;
	assert(n==m);
	double sum = 0;
	for(int i = 0; i<n; i++) {
		sum += gsl_vector_get(a,i)*gsl_vector_get(b,i);
	}
	sum *= alpha;

	return sum;
}
