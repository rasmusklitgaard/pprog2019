#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<assert.h>
#include<math.h>
#include"eigen.h"
#include"eig_exam.h"
#include"qr.h"
int nearest_eigenvalue(gsl_matrix* A, gsl_vector* x0, double* s, double epsrel, double epsabs)
{
	int n = A->size1;
	assert(n == x0->size);
	gsl_matrix* Acopy = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_matrix* idt = gsl_matrix_alloc(n,n);
	gsl_vector* x1 = gsl_vector_alloc(n);
	gsl_vector* sx1 = gsl_vector_alloc(n);
	gsl_vector* Ax1 = gsl_vector_alloc(n);

	gsl_matrix_set_identity(idt);
	gsl_matrix_memcpy(Acopy,A);

	// Normalizing x0
	//gsl_vector_scale(x0,1.0/calc_norm(x0));

	double eigenvalue = *s;
	int iter = 0;
	do
	{
		iter++;
		gsl_matrix_scale(idt,*s);
		gsl_matrix_sub(A,idt); // Now A = (A_copy - idt*s)
		gsl_matrix_set_identity(idt);
		qr_gs_decomp(A,R); // Now A is Q, so A*R = (A_copy - idt*s)
		
		// Solving system for x1
		qr_gs_solve(A,R,x0,x1);
		//gsl_vector_scale(x1,1.0/calc_norm(x1));
		
		//restoring A
		gsl_matrix_memcpy(A,Acopy);

		eigenvalue = rayleigh_eigval(A,x0);
		
		gsl_blas_dgemv(CblasNoTrans,1.0,A,x1,0.0,Ax1);
		gsl_vector_set_zero(sx1);
		gsl_blas_daxpy(eigenvalue,x1,sx1);
		gsl_vector_memcpy(x0,x1);
		
	//} while(iter < maxiter);
	} while((vector_equal(Ax1,sx1,epsrel,epsabs) != 1));

	gsl_vector_scale(x0,1/calc_norm(x0));
	*s = eigenvalue;
	
	gsl_matrix_free(Acopy);
	gsl_matrix_free(R);
	gsl_matrix_free(idt);
	gsl_vector_free(x1);
	gsl_vector_free(sx1);
	gsl_vector_free(Ax1);
	return iter;
}
double rayleigh_eigval(gsl_matrix* A, gsl_vector* xi)
{
	int n = xi->size;
	gsl_vector* tmp = gsl_vector_alloc(n);
	double upper;
	gsl_blas_dgemv(CblasNoTrans,1.0,A,xi,0.0,tmp);
	gsl_blas_ddot(xi,tmp,&upper);

	double lower = pow(calc_norm(xi),2);
	double res = upper / lower;
	return res;
}
double calc_norm(gsl_vector* v)
{
	double sum = 0;
	int n = v->size;
	for(int i = 0; i<n; i++) {
		double vi = gsl_vector_get(v,i);
		sum += vi*vi;
	}
	return sqrt(sum);
	
}
