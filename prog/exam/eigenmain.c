#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"eigen.h"
#define EPS 1e-8
int main(void)
{
	int n = 5;
	int no_eigens = 3;

	gsl_matrix* A_copy = gsl_matrix_alloc(n,n);
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* VTAV = gsl_matrix_alloc(n,n);
	gsl_matrix* VTA = gsl_matrix_alloc(n,n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	gsl_matrix* D = gsl_matrix_alloc(n,n);
	gsl_vector* e = gsl_vector_alloc(n);
	gsl_vector* Vi = gsl_vector_alloc(n);
	gsl_vector* AVi = gsl_vector_alloc(n);
	gsl_vector* eig_value_by_value = gsl_vector_alloc(no_eigens);
	
	
	for(int i = 0; i < n; i++)
	for(int j = i; j < n; j++)
	{
		double rv = ((double) random())/RAND_MAX * 50;
		gsl_matrix_set(A,i,j,rv);
		gsl_matrix_set(A,j,i,rv);
	}
	
	
	/*
	gsl_matrix_set(A,0,0,5);	
	gsl_matrix_set(A,0,1,7);	
	gsl_matrix_set(A,1,0,7);	
	gsl_matrix_set(A,0,2,3);	
	gsl_matrix_set(A,2,0,3);	
	gsl_matrix_set(A,1,1,1);	
	gsl_matrix_set(A,1,2,4);	
	gsl_matrix_set(A,2,1,4);	
	gsl_matrix_set(A,2,2,6);	
	*/

	printf("Matrix A:\n");
	matrix_print(A);
	printf("Doing jacobi diagonalization\n");
	int sweeps = jcbi_diag(A, e, V);
	printf("Done in %d sweeps\n",sweeps);
	matrix_print(A);
	printf("Matrix V:\n");
	matrix_print(V);
	printf("Vector e:\n");
	vector_print(e);

	//restoring A
	for(int i = 0; i < n; i++)
	for(int j = i + 1; j<n; j++) {
		gsl_matrix_set(A, i, j, gsl_matrix_get(A,j,i));
	}
/*	printf("\nOriginal A:\n");
	matrix_print(A);
	printf("\n");
*/

	printf("\nTesting if A*V0 = e0*V0\n");
	printf("It should be:\n");
	gsl_matrix_get_col(Vi,V,0);
	double e0 = gsl_vector_get(e,0);
	gsl_vector_scale(Vi,e0);
	vector_print(Vi);
	gsl_vector_scale(Vi,1/e0);
	printf("It is:\n");
	gsl_blas_dgemv(CblasNoTrans,1.0,A,Vi,0.0,AVi);
	vector_print(AVi);
	printf("It appears to be in order\n\n");
	printf("Checking that A*Vi = ei * Vi for all i\n");
	int equal = 0;
	for(int i = 0; i < n; i++) {

		gsl_matrix_get_col(Vi,V,i);
		gsl_blas_dgemv(CblasNoTrans, 1.0, A, Vi, 0.0, AVi);

		double ei = gsl_vector_get(e,i);
		gsl_vector_scale(Vi,ei);
 
		if(vector_equal(AVi,Vi,EPS) != 1) equal += 1;

		gsl_vector_scale(Vi,1/ei);

	}
	printf("According to my function vector_equal function: %i of the eigenvector equations\n",equal);
	printf("did not hold. (A * Vi was not equal to ei * Vi for %i i's)\n",equal);
	printf("To clarify: if 0 equations did not hold, everything went as planned\n");
	
	printf("\nChecking that V.T * A * V = D\n");
	gsl_blas_dgemm(CblasTrans,CblasNoTrans, 1.0, V, A, 0.0, VTA);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans, 1.0, VTA, V, 0.0, VTAV); 
	
	gsl_matrix_set_zero(D);
	for(int i = 0; i<n; i++) gsl_matrix_set(D,i,i, gsl_vector_get(e,i));
	if(matrix_equal(D,VTAV,EPS) == 1) printf("V^T * A * V is equal to D \n");
	else 			      printf("V^T * A * V is not equal to D\n");

	printf("_______________________________________________________\n");
	printf("Doing part b) \n");
	printf("Matrix A:\n");

	gsl_matrix_memcpy(A_copy,A);
	matrix_print(A);
	printf("Finding %i smallest eigenvalues\n",no_eigens);
	jacobi_value_by_value(A,eig_value_by_value,no_eigens,1);
	printf("The %i smallest eigenvalues are\n",no_eigens);
	vector_print(eig_value_by_value);

	printf("Finding %i biggest eigenvalues\n",no_eigens);
	jacobi_value_by_value(A_copy,eig_value_by_value,no_eigens,-1);
	printf("The %i biggest eigenvalues are\n",no_eigens);
	vector_print(eig_value_by_value);

	printf("Comparing these to the ones previously found (which has been tested to be OK)\n");
	vector_print(e);
	printf("This looks to be in order\n");



	gsl_matrix_free(A_copy);
	gsl_matrix_free(A);
	gsl_matrix_free(D);
	gsl_matrix_free(VTAV);
	gsl_matrix_free(VTA);
	gsl_matrix_free(V);
	gsl_vector_free(e);
	gsl_vector_free(Vi);
	gsl_vector_free(AVi);
	gsl_vector_free(eig_value_by_value);
	return 0;
}

