#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"eig_exam.h"
#include"eigen.h"
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
void random_symmetric_matrix(gsl_matrix* A,int n);
int main(void)
{
	int n = 7;
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* Acopy = gsl_matrix_alloc(n,n);
	gsl_matrix* vecs = gsl_matrix_alloc(n,n);
	gsl_vector* x0= gsl_vector_alloc(n);
	gsl_vector* vals= gsl_vector_alloc(n);
	gsl_vector* real_vec= gsl_vector_alloc(n);

	random_symmetric_matrix(A,n);
	gsl_matrix_memcpy(Acopy,A);
	gsl_vector_set(x0,0,1.9);
	gsl_vector_set(x0,1,14.9);
	gsl_vector_set(x0,2,-2.9);
	gsl_vector_set(x0,3,19.9);

	printf("Matrix A:\n");
	matrix_print(A);
	jcbi_diag(A,vals,vecs);
	printf("Doing jacobi diagonalization\n");
	gsl_matrix_memcpy(A,Acopy);
	printf("Matrix A\n");
	matrix_print(A);
	printf("Eigenvalues\n");
	vector_print(vals);
	printf("Eigenvectors\n");
	matrix_print(vecs);


	double epsrel = 1e-6;
	double epsabs = 1e-6;
	
	unsigned int seed = time(NULL);

	int valuenr =(int) (((double)rand_r(&seed))/RAND_MAX*n);
	double fraction = (double) rand_r(&seed)/RAND_MAX/2-0.25; // up to 25% difference
	double guess = gsl_vector_get(vals,valuenr);
	guess = guess + fraction*guess;


	gsl_vector_view vec_temp = gsl_matrix_column(vecs,valuenr);
	*real_vec = vec_temp.vector;

	double oldguess = guess;
	printf("Looking for the nr %i eigenvalue, and choosing %g as guess\n",valuenr,guess);
	
	int iter = nearest_eigenvalue(A,x0,&guess,epsrel,epsabs);


	printf("Our guess was: %g\n",oldguess);
	printf("The nearest eigenvalue is: %g\n",guess);
	printf("It should converge to %g\n",gsl_vector_get(vals,valuenr));
	printf("The eigenvector for this eigenvalue is obtained to be:\n");
	vector_print(x0);
	printf("It should be:\n");
	vector_print(real_vec);
	
	printf("It was obtained in %i iterations\n",iter);
	
	gsl_matrix_free(A);
	gsl_matrix_free(Acopy);
	gsl_matrix_free(vecs);
	gsl_vector_free(x0);
	gsl_vector_free(vals);
	gsl_vector_free(real_vec);
	return 0;
}
void random_symmetric_matrix(gsl_matrix* A, int n)
{
	for(int i = 0; i < n; i++)
	for(int j = i; j < n; j++)
	{
		double rv = ((double) random())/RAND_MAX * 50;
		gsl_matrix_set(A,i,j,rv);
		gsl_matrix_set(A,j,i,rv);
	}
}
