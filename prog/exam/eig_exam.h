#include<gsl/gsl_matrix.h>
int nearest_eigenvalue(gsl_matrix* A, gsl_vector* x0, double* s, double epsrel, double epsabs);
double rayleigh_eigval(gsl_matrix* A, gsl_vector* xi);
double calc_norm(gsl_vector* v);
