#include<gsl/gsl_matrix.h>
int jcbi_diag(gsl_matrix* A, gsl_vector* vals, gsl_matrix* vecs);
void jacobi_value_by_value(gsl_matrix* A, gsl_vector* eig, int no_of_eigenvalues, int sort);
void vector_print(gsl_vector* b);
void matrix_print(gsl_matrix* A);
int matrix_equal(gsl_matrix* a, gsl_matrix* b,double eps);
int vector_equal(gsl_vector* a, gsl_vector* b,double epsrel,double epsabs);
